/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.palmdrivermotorista.driver;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.palmdrivermotorista.driver";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.0";
  // Fields from default config.
  public static final String BASE_IMAGE_URL = "https://palmdriver.com.br/storage/";
  public static final String BASE_PAY_URL = "https://palmdriver.com.br/index.php";
  public static final String BASE_URL = "https://palmdriver.com.br/";
  public static final String CLIENT_ID = "2";
  public static final String CLIENT_SECRET = "taHdKq3goXAkwavriUFZQVXbHag1AxyJseNCVgiE";
  public static final String DEVICE_TYPE = "android";
  public static final String FCM_SERRVER_KEY = "AAAAwhJbaCc:APA91bG9wrUhjnExHzDD9vLB1iwm0n7B9BSpGNZpyu0wRVYsWGUm5GPKGo14UwUWNX3pvYGYs3lR24Pb7XvKDpLDnzkkqeElNiWdsi7BA2QCuTscMH7LZe1RBSzy_nh1TH0c4gDlD7Gr";
  public static final String HELP_URL = "https://palmdriver.com.br/help";
  public static final String TERMS_CONDITIONS = "https://palmdriver.com.br/privacy";
  public static final String USER_PACKAGE = "com.palmdrivermotorista.user";
}
