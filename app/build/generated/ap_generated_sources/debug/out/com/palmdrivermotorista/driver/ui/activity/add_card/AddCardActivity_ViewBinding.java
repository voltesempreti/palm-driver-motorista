// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.add_card;

import android.view.View;
import android.widget.Button;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddCardActivity_ViewBinding implements Unbinder {
  private AddCardActivity target;

  private View view7f0a02c8;

  @UiThread
  public AddCardActivity_ViewBinding(AddCardActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddCardActivity_ViewBinding(final AddCardActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.submit, "field 'submit' and method 'onViewClicked'");
    target.submit = Utils.castView(view, R.id.submit, "field 'submit'", Button.class);
    view7f0a02c8 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AddCardActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.submit = null;

    view7f0a02c8.setOnClickListener(null);
    view7f0a02c8 = null;
  }
}
