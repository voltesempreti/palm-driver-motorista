// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.payment;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PaymentActivity_ViewBinding implements Unbinder {
  private PaymentActivity target;

  private View view7f0a0093;

  private View view7f0a0092;

  private View view7f0a004b;

  @UiThread
  public PaymentActivity_ViewBinding(PaymentActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PaymentActivity_ViewBinding(final PaymentActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.cash, "field 'tvCash' and method 'onViewClicked'");
    target.tvCash = Utils.castView(view, R.id.cash, "field 'tvCash'", TextView.class);
    view7f0a0093 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.cards_rv, "field 'cardsRv' and method 'onViewClicked'");
    target.cardsRv = Utils.castView(view, R.id.cards_rv, "field 'cardsRv'", RecyclerView.class);
    view7f0a0092 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.llCardContainer = Utils.findRequiredViewAsType(source, R.id.llCardContainer, "field 'llCardContainer'", LinearLayout.class);
    target.llCashContainer = Utils.findRequiredViewAsType(source, R.id.llCashContainer, "field 'llCashContainer'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.addCard, "field 'addCard' and method 'onViewClicked'");
    target.addCard = Utils.castView(view, R.id.addCard, "field 'addCard'", Button.class);
    view7f0a004b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    PaymentActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvCash = null;
    target.cardsRv = null;
    target.llCardContainer = null;
    target.llCashContainer = null;
    target.addCard = null;

    view7f0a0093.setOnClickListener(null);
    view7f0a0093 = null;
    view7f0a0092.setOnClickListener(null);
    view7f0a0092 = null;
    view7f0a004b.setOnClickListener(null);
    view7f0a004b = null;
  }
}
