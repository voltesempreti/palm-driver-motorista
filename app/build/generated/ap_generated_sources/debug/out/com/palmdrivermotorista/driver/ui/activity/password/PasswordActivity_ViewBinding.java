// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.password;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PasswordActivity_ViewBinding implements Unbinder {
  private PasswordActivity target;

  private View view7f0a0063;

  private View view7f0a02a1;

  private View view7f0a0147;

  private View view7f0a020f;

  @UiThread
  public PasswordActivity_ViewBinding(PasswordActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PasswordActivity_ViewBinding(final PasswordActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.back, "field 'back' and method 'onViewClicked'");
    target.back = Utils.castView(view, R.id.back, "field 'back'", ImageView.class);
    view7f0a0063 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.password = Utils.findRequiredViewAsType(source, R.id.password, "field 'password'", TextInputEditText.class);
    view = Utils.findRequiredView(source, R.id.sign_up, "field 'signUp' and method 'onViewClicked'");
    target.signUp = Utils.castView(view, R.id.sign_up, "field 'signUp'", TextView.class);
    view7f0a02a1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.forgot_password, "field 'forgotPassword' and method 'onViewClicked'");
    target.forgotPassword = Utils.castView(view, R.id.forgot_password, "field 'forgotPassword'", TextView.class);
    view7f0a0147 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.next, "field 'next' and method 'onViewClicked'");
    target.next = Utils.castView(view, R.id.next, "field 'next'", FloatingActionButton.class);
    view7f0a020f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    PasswordActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.back = null;
    target.password = null;
    target.signUp = null;
    target.forgotPassword = null;
    target.next = null;

    view7f0a0063.setOnClickListener(null);
    view7f0a0063 = null;
    view7f0a02a1.setOnClickListener(null);
    view7f0a02a1 = null;
    view7f0a0147.setOnClickListener(null);
    view7f0a0147 = null;
    view7f0a020f.setOnClickListener(null);
    view7f0a020f = null;
  }
}
