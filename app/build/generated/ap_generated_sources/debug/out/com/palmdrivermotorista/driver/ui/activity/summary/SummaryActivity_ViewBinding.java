// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.summary;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SummaryActivity_ViewBinding implements Unbinder {
  private SummaryActivity target;

  @UiThread
  public SummaryActivity_ViewBinding(SummaryActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SummaryActivity_ViewBinding(SummaryActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.cardLayout = Utils.findRequiredViewAsType(source, R.id.card_layout, "field 'cardLayout'", LinearLayout.class);
    target.ridesValue = Utils.findRequiredViewAsType(source, R.id.rides_value, "field 'ridesValue'", TextView.class);
    target.ridesCard = Utils.findRequiredViewAsType(source, R.id.rides_card, "field 'ridesCard'", CardView.class);
    target.completedRidesValue = Utils.findRequiredViewAsType(source, R.id.completed_rides_value, "field 'completedRidesValue'", TextView.class);
    target.completedRides = Utils.findRequiredViewAsType(source, R.id.completed_rides_card, "field 'completedRides'", CardView.class);
    target.revenueValue = Utils.findRequiredViewAsType(source, R.id.revenue_value, "field 'revenueValue'", TextView.class);
    target.revenueCard = Utils.findRequiredViewAsType(source, R.id.revenue_card, "field 'revenueCard'", CardView.class);
    target.montlyGainsValue = Utils.findRequiredViewAsType(source, R.id.montly_gains_value, "field 'montlyGainsValue'", TextView.class);
    target.montlyGainsCard = Utils.findRequiredViewAsType(source, R.id.montly_gains_card, "field 'montlyGainsCard'", CardView.class);
    target.montlyPassValue = Utils.findRequiredViewAsType(source, R.id.montly_pass_value, "field 'montlyPassValue'", TextView.class);
    target.montlyPassCard = Utils.findRequiredViewAsType(source, R.id.montly_pass_card, "field 'montlyPassCard'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SummaryActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.cardLayout = null;
    target.ridesValue = null;
    target.ridesCard = null;
    target.completedRidesValue = null;
    target.completedRides = null;
    target.revenueValue = null;
    target.revenueCard = null;
    target.montlyGainsValue = null;
    target.montlyGainsCard = null;
    target.montlyPassValue = null;
    target.montlyPassCard = null;
  }
}
