// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.profile;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfileActivity_ViewBinding implements Unbinder {
  private ProfileActivity target;

  private View view7f0a0136;

  @UiThread
  public ProfileActivity_ViewBinding(ProfileActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProfileActivity_ViewBinding(final ProfileActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.imgProfile = Utils.findRequiredViewAsType(source, R.id.imgProfile, "field 'imgProfile'", CircularImageView.class);
    target.completeData = Utils.findRequiredViewAsType(source, R.id.completeData, "field 'completeData'", LinearLayout.class);
    target.txtName = Utils.findRequiredViewAsType(source, R.id.nameProfile, "field 'txtName'", TextView.class);
    target.txtCpf = Utils.findRequiredViewAsType(source, R.id.cpfProfile, "field 'txtCpf'", TextView.class);
    target.txtPhoneNumber = Utils.findRequiredViewAsType(source, R.id.mobileProfile, "field 'txtPhoneNumber'", TextView.class);
    target.txtEmail = Utils.findRequiredViewAsType(source, R.id.emailProfile, "field 'txtEmail'", TextView.class);
    target.txtService = Utils.findRequiredViewAsType(source, R.id.serviceProfile, "field 'txtService'", TextView.class);
    target.txtModel = Utils.findRequiredViewAsType(source, R.id.modelVeicleProfile, "field 'txtModel'", TextView.class);
    target.txtNumber = Utils.findRequiredViewAsType(source, R.id.boardProfile, "field 'txtNumber'", TextView.class);
    view = Utils.findRequiredView(source, R.id.fabEdit, "field 'edit' and method 'onViewClicked'");
    target.edit = Utils.castView(view, R.id.fabEdit, "field 'edit'", FloatingActionButton.class);
    view7f0a0136 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ProfileActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.imgProfile = null;
    target.completeData = null;
    target.txtName = null;
    target.txtCpf = null;
    target.txtPhoneNumber = null;
    target.txtEmail = null;
    target.txtService = null;
    target.txtModel = null;
    target.txtNumber = null;
    target.edit = null;

    view7f0a0136.setOnClickListener(null);
    view7f0a0136 = null;
  }
}
