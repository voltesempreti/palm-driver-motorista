// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.main;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.gauravbhola.ripplepulsebackground.RipplePulseLayout;
import com.google.android.material.navigation.NavigationView;
import com.palmdrivermotorista.driver.R;
import ir.sohreco.circularpulsingbutton.CircularPulsingButton;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  private View view7f0a01cf;

  private View view7f0a0207;

  private View view7f0a0151;

  private View view7f0a020b;

  private View view7f0a027e;

  private View view7f0a02c6;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(final MainActivity target, View source) {
    this.target = target;

    View view;
    target.container = Utils.findRequiredViewAsType(source, R.id.container, "field 'container'", FrameLayout.class);
    view = Utils.findRequiredView(source, R.id.menu, "field 'menu' and method 'onViewClicked'");
    target.menu = Utils.castView(view, R.id.menu, "field 'menu'", ImageView.class);
    view7f0a01cf = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.drawerLayout = Utils.findRequiredViewAsType(source, R.id.drawer_layout, "field 'drawerLayout'", DrawerLayout.class);
    target.topLayout = Utils.findRequiredViewAsType(source, R.id.top_layout, "field 'topLayout'", LinearLayout.class);
    target.lnrLocationHeader = Utils.findRequiredViewAsType(source, R.id.lnrLocationHeader, "field 'lnrLocationHeader'", LinearLayout.class);
    target.lblLocationType = Utils.findRequiredViewAsType(source, R.id.lblLocationType, "field 'lblLocationType'", TextView.class);
    target.lblLocationName = Utils.findRequiredViewAsType(source, R.id.lblLocationName, "field 'lblLocationName'", TextView.class);
    target.offlineContainer = Utils.findRequiredViewAsType(source, R.id.offline_container, "field 'offlineContainer'", FrameLayout.class);
    view = Utils.findRequiredView(source, R.id.nav_view, "field 'navView' and method 'onViewClicked'");
    target.navView = Utils.castView(view, R.id.nav_view, "field 'navView'", NavigationView.class);
    view7f0a0207 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.gps, "field 'gps' and method 'onViewClicked'");
    target.gps = Utils.castView(view, R.id.gps, "field 'gps'", ImageView.class);
    view7f0a0151 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.navigation_img, "field 'navigationImg' and method 'onViewClicked'");
    target.navigationImg = Utils.castView(view, R.id.navigation_img, "field 'navigationImg'", ImageView.class);
    view7f0a020b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sbChangeStatus, "field 'sbChangeStatus' and method 'onViewClicked'");
    target.sbChangeStatus = Utils.castView(view, R.id.sbChangeStatus, "field 'sbChangeStatus'", CircularPulsingButton.class);
    view7f0a027e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.style_map, "field 'styleMapButton' and method 'onViewClicked'");
    target.styleMapButton = Utils.castView(view, R.id.style_map, "field 'styleMapButton'", ImageView.class);
    view7f0a02c6 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutRipplepulse = Utils.findRequiredViewAsType(source, R.id.layout_ripplepulse, "field 'layoutRipplepulse'", RipplePulseLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.container = null;
    target.menu = null;
    target.drawerLayout = null;
    target.topLayout = null;
    target.lnrLocationHeader = null;
    target.lblLocationType = null;
    target.lblLocationName = null;
    target.offlineContainer = null;
    target.navView = null;
    target.gps = null;
    target.navigationImg = null;
    target.sbChangeStatus = null;
    target.styleMapButton = null;
    target.layoutRipplepulse = null;

    view7f0a01cf.setOnClickListener(null);
    view7f0a01cf = null;
    view7f0a0207.setOnClickListener(null);
    view7f0a0207 = null;
    view7f0a0151.setOnClickListener(null);
    view7f0a0151 = null;
    view7f0a020b.setOnClickListener(null);
    view7f0a020b = null;
    view7f0a027e.setOnClickListener(null);
    view7f0a027e = null;
    view7f0a02c6.setOnClickListener(null);
    view7f0a02c6 = null;
  }
}
