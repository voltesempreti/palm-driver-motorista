// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.fragment.status_flow;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatRatingBar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StatusFlowFragment_ViewBinding implements Unbinder {
  private StatusFlowFragment target;

  private View view7f0a0164;

  private View view7f0a0075;

  private View view7f0a007f;

  private View view7f0a0167;

  private View view7f0a0072;

  private View view7f0a02ad;

  @UiThread
  public StatusFlowFragment_ViewBinding(final StatusFlowFragment target, View source) {
    this.target = target;

    View view;
    target.userName = Utils.findRequiredViewAsType(source, R.id.user_name, "field 'userName'", TextView.class);
    target.userRating = Utils.findRequiredViewAsType(source, R.id.user_rating, "field 'userRating'", AppCompatRatingBar.class);
    view = Utils.findRequiredView(source, R.id.imgCall, "field 'imgCall' and method 'onViewClicked'");
    target.imgCall = Utils.castView(view, R.id.imgCall, "field 'imgCall'", ImageView.class);
    view7f0a0164 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btnCancel, "field 'btnCancel' and method 'onViewClicked'");
    target.btnCancel = Utils.castView(view, R.id.btnCancel, "field 'btnCancel'", Button.class);
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btnStatus, "field 'btnStatus' and method 'onViewClicked'");
    target.btnStatus = Utils.castView(view, R.id.btnStatus, "field 'btnStatus'", Button.class);
    view7f0a007f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.statusArrivedImg = Utils.findRequiredViewAsType(source, R.id.status_arrived_img, "field 'statusArrivedImg'", ImageView.class);
    target.statusPickedUpImg = Utils.findRequiredViewAsType(source, R.id.status_picked_up_img, "field 'statusPickedUpImg'", ImageView.class);
    target.statusFinishedImg = Utils.findRequiredViewAsType(source, R.id.status_finished_img, "field 'statusFinishedImg'", ImageView.class);
    target.userImg = Utils.findRequiredViewAsType(source, R.id.user_img, "field 'userImg'", CircleImageView.class);
    view = Utils.findRequiredView(source, R.id.imgMsg, "field 'imgMsg' and method 'onViewClicked'");
    target.imgMsg = Utils.castView(view, R.id.imgMsg, "field 'imgMsg'", ImageView.class);
    view7f0a0167 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.arrivedView = Utils.findRequiredView(source, R.id.arrived_view, "field 'arrivedView'");
    view = Utils.findRequiredView(source, R.id.btWaitingTime, "field 'btWaitingTime' and method 'onViewClicked'");
    target.btWaitingTime = Utils.castView(view, R.id.btWaitingTime, "field 'btWaitingTime'", Button.class);
    view7f0a0072 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.tvTimer = Utils.findRequiredViewAsType(source, R.id.tvTimer, "field 'tvTimer'", TextView.class);
    target.llWaitingTimeContainer = Utils.findRequiredViewAsType(source, R.id.llWaitingTimeContainer, "field 'llWaitingTimeContainer'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.sos, "method 'onViewClicked'");
    view7f0a02ad = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    StatusFlowFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.userName = null;
    target.userRating = null;
    target.imgCall = null;
    target.btnCancel = null;
    target.btnStatus = null;
    target.statusArrivedImg = null;
    target.statusPickedUpImg = null;
    target.statusFinishedImg = null;
    target.userImg = null;
    target.imgMsg = null;
    target.arrivedView = null;
    target.btWaitingTime = null;
    target.tvTimer = null;
    target.llWaitingTimeContainer = null;

    view7f0a0164.setOnClickListener(null);
    view7f0a0164 = null;
    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
    view7f0a007f.setOnClickListener(null);
    view7f0a007f = null;
    view7f0a0167.setOnClickListener(null);
    view7f0a0167 = null;
    view7f0a0072.setOnClickListener(null);
    view7f0a0072 = null;
    view7f0a02ad.setOnClickListener(null);
    view7f0a02ad = null;
  }
}
