// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.fragment.dispute_status;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DisputeStatusFragment_ViewBinding implements Unbinder {
  private DisputeStatusFragment target;

  @UiThread
  public DisputeStatusFragment_ViewBinding(DisputeStatusFragment target, View source) {
    this.target = target;

    target.userDispute = Utils.findRequiredViewAsType(source, R.id.user_dispute, "field 'userDispute'", TextView.class);
    target.adminComment = Utils.findRequiredViewAsType(source, R.id.admin_comment, "field 'adminComment'", TextView.class);
    target.disputeStatus = Utils.findRequiredViewAsType(source, R.id.dispute_status, "field 'disputeStatus'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DisputeStatusFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.userDispute = null;
    target.adminComment = null;
    target.disputeStatus = null;
  }
}
