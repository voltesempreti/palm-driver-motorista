// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.wallet;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WalletActivity_ViewBinding implements Unbinder {
  private WalletActivity target;

  private View view7f0a0180;

  private View view7f0a004a;

  @UiThread
  public WalletActivity_ViewBinding(WalletActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WalletActivity_ViewBinding(final WalletActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.tvWalletAmt = Utils.findRequiredViewAsType(source, R.id.tvWalletAmt, "field 'tvWalletAmt'", TextView.class);
    target.rvWalletData = Utils.findRequiredViewAsType(source, R.id.rvWalletData, "field 'rvWalletData'", RecyclerView.class);
    target.tvWalletPlaceholder = Utils.findRequiredViewAsType(source, R.id.tvWalletPlaceholder, "field 'tvWalletPlaceholder'", TextView.class);
    target.llWalletHistory = Utils.findRequiredViewAsType(source, R.id.llWalletHistory, "field 'llWalletHistory'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.ivRequestMoney, "field 'ivRequestMoney' and method 'onViewClicked'");
    target.ivRequestMoney = Utils.castView(view, R.id.ivRequestMoney, "field 'ivRequestMoney'", TextView.class);
    view7f0a0180 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.addAmount, "field 'addAmount' and method 'onViewClicked'");
    target.addAmount = Utils.castView(view, R.id.addAmount, "field 'addAmount'", Button.class);
    view7f0a004a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etRequestAmt = Utils.findRequiredViewAsType(source, R.id.etRequestAmt, "field 'etRequestAmt'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    WalletActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.tvWalletAmt = null;
    target.rvWalletData = null;
    target.tvWalletPlaceholder = null;
    target.llWalletHistory = null;
    target.ivRequestMoney = null;
    target.addAmount = null;
    target.etRequestAmt = null;

    view7f0a0180.setOnClickListener(null);
    view7f0a0180 = null;
    view7f0a004a.setOnClickListener(null);
    view7f0a004a = null;
  }
}
