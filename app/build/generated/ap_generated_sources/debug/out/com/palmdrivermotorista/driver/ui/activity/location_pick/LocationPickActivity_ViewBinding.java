// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.location_pick;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.appbar.AppBarLayout;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LocationPickActivity_ViewBinding implements Unbinder {
  private LocationPickActivity target;

  private View view7f0a02ae;

  private View view7f0a00fe;

  private View view7f0a015a;

  private View view7f0a0354;

  private View view7f0a0264;

  private View view7f0a0263;

  @UiThread
  public LocationPickActivity_ViewBinding(LocationPickActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LocationPickActivity_ViewBinding(final LocationPickActivity target, View source) {
    this.target = target;

    View view;
    target.appbar = Utils.findRequiredViewAsType(source, R.id.appbar, "field 'appbar'", AppBarLayout.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    view = Utils.findRequiredView(source, R.id.source, "field 'source' and method 'onViewClicked'");
    target.source = Utils.castView(view, R.id.source, "field 'source'", EditText.class);
    view7f0a02ae = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.destination, "field 'destination' and method 'onViewClicked'");
    target.destination = Utils.castView(view, R.id.destination, "field 'destination'", EditText.class);
    view7f0a00fe = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.destinationLayout = Utils.findRequiredViewAsType(source, R.id.destination_layout, "field 'destinationLayout'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.home_address_layout, "field 'homeAddressLayout' and method 'onViewClicked'");
    target.homeAddressLayout = Utils.castView(view, R.id.home_address_layout, "field 'homeAddressLayout'", LinearLayout.class);
    view7f0a015a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.work_address_layout, "field 'workAddressLayout' and method 'onViewClicked'");
    target.workAddressLayout = Utils.castView(view, R.id.work_address_layout, "field 'workAddressLayout'", LinearLayout.class);
    view7f0a0354 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.homeAddress = Utils.findRequiredViewAsType(source, R.id.home_address, "field 'homeAddress'", TextView.class);
    target.workAddress = Utils.findRequiredViewAsType(source, R.id.work_address, "field 'workAddress'", TextView.class);
    target.locationsRv = Utils.findRequiredViewAsType(source, R.id.locations_rv, "field 'locationsRv'", RecyclerView.class);
    target.locationBsLayout = Utils.findRequiredViewAsType(source, R.id.location_bs_layout, "field 'locationBsLayout'", CardView.class);
    target.dd = Utils.findRequiredViewAsType(source, R.id.dd, "field 'dd'", CoordinatorLayout.class);
    view = Utils.findRequiredView(source, R.id.reset_source, "method 'onViewClicked'");
    view7f0a0264 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.reset_destination, "method 'onViewClicked'");
    view7f0a0263 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LocationPickActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.appbar = null;
    target.toolbar = null;
    target.source = null;
    target.destination = null;
    target.destinationLayout = null;
    target.homeAddressLayout = null;
    target.workAddressLayout = null;
    target.homeAddress = null;
    target.workAddress = null;
    target.locationsRv = null;
    target.locationBsLayout = null;
    target.dd = null;

    view7f0a02ae.setOnClickListener(null);
    view7f0a02ae = null;
    view7f0a00fe.setOnClickListener(null);
    view7f0a00fe = null;
    view7f0a015a.setOnClickListener(null);
    view7f0a015a = null;
    view7f0a0354.setOnClickListener(null);
    view7f0a0354 = null;
    view7f0a0264.setOnClickListener(null);
    view7f0a0264 = null;
    view7f0a0263.setOnClickListener(null);
    view7f0a0263 = null;
  }
}
