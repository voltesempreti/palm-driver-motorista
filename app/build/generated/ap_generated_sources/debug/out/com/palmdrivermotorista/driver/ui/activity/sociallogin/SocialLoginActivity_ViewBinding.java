// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.sociallogin;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SocialLoginActivity_ViewBinding implements Unbinder {
  private SocialLoginActivity target;

  private View view7f0a0063;

  private View view7f0a0196;

  @UiThread
  public SocialLoginActivity_ViewBinding(SocialLoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SocialLoginActivity_ViewBinding(final SocialLoginActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.back, "field 'back' and method 'onViewClicked'");
    target.back = Utils.castView(view, R.id.back, "field 'back'", ImageView.class);
    view7f0a0063 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.title = Utils.findRequiredViewAsType(source, R.id.title, "field 'title'", TextView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    view = Utils.findRequiredView(source, R.id.lblFacebook, "field 'lblFacebook' and method 'onViewClicked'");
    target.lblFacebook = Utils.castView(view, R.id.lblFacebook, "field 'lblFacebook'", TextView.class);
    view7f0a0196 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SocialLoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.back = null;
    target.title = null;
    target.toolbar = null;
    target.lblFacebook = null;

    view7f0a0063.setOnClickListener(null);
    view7f0a0063 = null;
    view7f0a0196.setOnClickListener(null);
    view7f0a0196 = null;
  }
}
