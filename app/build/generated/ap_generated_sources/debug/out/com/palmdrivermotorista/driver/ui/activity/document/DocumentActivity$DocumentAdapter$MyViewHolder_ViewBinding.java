// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.document;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DocumentActivity$DocumentAdapter$MyViewHolder_ViewBinding implements Unbinder {
  private DocumentActivity.DocumentAdapter.MyViewHolder target;

  private View view7f0a0070;

  @UiThread
  public DocumentActivity$DocumentAdapter$MyViewHolder_ViewBinding(
      final DocumentActivity.DocumentAdapter.MyViewHolder target, View source) {
    this.target = target;

    View view;
    target.tvDocTitle = Utils.findRequiredViewAsType(source, R.id.tvDocTitle, "field 'tvDocTitle'", TextView.class);
    target.ivDocument = Utils.findRequiredViewAsType(source, R.id.ivDocument, "field 'ivDocument'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.btAddDocument, "field 'btAddDocument' and method 'onViewClicked'");
    target.btAddDocument = Utils.castView(view, R.id.btAddDocument, "field 'btAddDocument'", Button.class);
    view7f0a0070 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    DocumentActivity.DocumentAdapter.MyViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvDocTitle = null;
    target.ivDocument = null;
    target.btAddDocument = null;

    view7f0a0070.setOnClickListener(null);
    view7f0a0070 = null;
  }
}
