// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.earnings;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import com.palmdrivermotorista.driver.common.CircularProgressBar;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EarningsActivity_ViewBinding implements Unbinder {
  private EarningsActivity target;

  @UiThread
  public EarningsActivity_ViewBinding(EarningsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public EarningsActivity_ViewBinding(EarningsActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.earningsBar = Utils.findRequiredViewAsType(source, R.id.earnings_bar, "field 'earningsBar'", CircularProgressBar.class);
    target.targetTxt = Utils.findRequiredViewAsType(source, R.id.target_txt, "field 'targetTxt'", TextView.class);
    target.lblEarnings = Utils.findRequiredViewAsType(source, R.id.lblEarnings, "field 'lblEarnings'", TextView.class);
    target.ridesRv = Utils.findRequiredViewAsType(source, R.id.rides_rv, "field 'ridesRv'", RecyclerView.class);
    target.errorImage = Utils.findRequiredViewAsType(source, R.id.error_image, "field 'errorImage'", ImageView.class);
    target.errorLayout = Utils.findRequiredViewAsType(source, R.id.errorLayout, "field 'errorLayout'", RelativeLayout.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progress_bar, "field 'progressBar'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EarningsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.earningsBar = null;
    target.targetTxt = null;
    target.lblEarnings = null;
    target.ridesRv = null;
    target.errorImage = null;
    target.errorLayout = null;
    target.progressBar = null;
  }
}
