// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.bottomsheetdialog.invoice_show;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InvoiceShowDialogFragment_ViewBinding implements Unbinder {
  private InvoiceShowDialogFragment target;

  private View view7f0a0078;

  @UiThread
  public InvoiceShowDialogFragment_ViewBinding(final InvoiceShowDialogFragment target,
      View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.btnClose, "field 'btnClose' and method 'onViewClicked'");
    target.btnClose = Utils.castView(view, R.id.btnClose, "field 'btnClose'", Button.class);
    view7f0a0078 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.lblBookingid = Utils.findRequiredViewAsType(source, R.id.lblBookingid, "field 'lblBookingid'", TextView.class);
    target.lblDistanceTravelled = Utils.findRequiredViewAsType(source, R.id.lblDistanceTravelled, "field 'lblDistanceTravelled'", TextView.class);
    target.lblTimetaken = Utils.findRequiredViewAsType(source, R.id.lblTimetaken, "field 'lblTimetaken'", TextView.class);
    target.lblBasreFare = Utils.findRequiredViewAsType(source, R.id.lblBasreFare, "field 'lblBasreFare'", TextView.class);
    target.lblDistanceFare = Utils.findRequiredViewAsType(source, R.id.lblDistanceFare, "field 'lblDistanceFare'", TextView.class);
    target.lblTax = Utils.findRequiredViewAsType(source, R.id.lblTax, "field 'lblTax'", TextView.class);
    target.lblTips = Utils.findRequiredViewAsType(source, R.id.lblTips, "field 'lblTips'", TextView.class);
    target.lblTotal = Utils.findRequiredViewAsType(source, R.id.lblTotal, "field 'lblTotal'", TextView.class);
    target.distanceContainer = Utils.findRequiredViewAsType(source, R.id.distance_container, "field 'distanceContainer'", LinearLayout.class);
    target.lblTimeFare = Utils.findRequiredViewAsType(source, R.id.lblTimeFare, "field 'lblTimeFare'", TextView.class);
    target.timeContainer = Utils.findRequiredViewAsType(source, R.id.time_container, "field 'timeContainer'", LinearLayout.class);
    target.tipsLayout = Utils.findRequiredViewAsType(source, R.id.tips_layout, "field 'tipsLayout'", LinearLayout.class);
    target.llAmountToBePaid = Utils.findRequiredViewAsType(source, R.id.llAmountToBePaid, "field 'llAmountToBePaid'", LinearLayout.class);
    target.lblPaid = Utils.findRequiredViewAsType(source, R.id.lblPaid, "field 'lblPaid'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    InvoiceShowDialogFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnClose = null;
    target.lblBookingid = null;
    target.lblDistanceTravelled = null;
    target.lblTimetaken = null;
    target.lblBasreFare = null;
    target.lblDistanceFare = null;
    target.lblTax = null;
    target.lblTips = null;
    target.lblTotal = null;
    target.distanceContainer = null;
    target.lblTimeFare = null;
    target.timeContainer = null;
    target.tipsLayout = null;
    target.llAmountToBePaid = null;
    target.lblPaid = null;

    view7f0a0078.setOnClickListener(null);
    view7f0a0078 = null;
  }
}
