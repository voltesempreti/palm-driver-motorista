// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.notification_manager;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NotificationManagerActivity_ViewBinding implements Unbinder {
  private NotificationManagerActivity target;

  @UiThread
  public NotificationManagerActivity_ViewBinding(NotificationManagerActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public NotificationManagerActivity_ViewBinding(NotificationManagerActivity target, View source) {
    this.target = target;

    target.rvNotificationManager = Utils.findRequiredViewAsType(source, R.id.rvNotificationManager, "field 'rvNotificationManager'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    NotificationManagerActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvNotificationManager = null;
  }
}
