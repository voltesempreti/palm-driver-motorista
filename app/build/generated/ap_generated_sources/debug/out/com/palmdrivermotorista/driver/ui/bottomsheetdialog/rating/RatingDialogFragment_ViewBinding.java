// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.bottomsheetdialog.rating;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RatingDialogFragment_ViewBinding implements Unbinder {
  private RatingDialogFragment target;

  private View view7f0a0080;

  @UiThread
  public RatingDialogFragment_ViewBinding(final RatingDialogFragment target, View source) {
    this.target = target;

    View view;
    target.rateWithTxt = Utils.findRequiredViewAsType(source, R.id.rate_with_txt, "field 'rateWithTxt'", TextView.class);
    target.userImg = Utils.findRequiredViewAsType(source, R.id.user_img, "field 'userImg'", ImageView.class);
    target.userRating = Utils.findRequiredViewAsType(source, R.id.user_rating, "field 'userRating'", RatingBar.class);
    target.comments = Utils.findRequiredViewAsType(source, R.id.comments, "field 'comments'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btnSubmit, "field 'btnSubmit' and method 'onViewClicked'");
    target.btnSubmit = Utils.castView(view, R.id.btnSubmit, "field 'btnSubmit'", Button.class);
    view7f0a0080 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RatingDialogFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rateWithTxt = null;
    target.userImg = null;
    target.userRating = null;
    target.comments = null;
    target.btnSubmit = null;

    view7f0a0080.setOnClickListener(null);
    view7f0a0080 = null;
  }
}
