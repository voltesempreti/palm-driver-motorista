// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.document;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DocumentActivity_ViewBinding implements Unbinder {
  private DocumentActivity target;

  private View view7f0a0071;

  @UiThread
  public DocumentActivity_ViewBinding(DocumentActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public DocumentActivity_ViewBinding(final DocumentActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.rvDocuments = Utils.findRequiredViewAsType(source, R.id.rvDocuments, "field 'rvDocuments'", RecyclerView.class);
    target.tvNoDocument = Utils.findRequiredViewAsType(source, R.id.tvNoDocument, "field 'tvNoDocument'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btProceed, "method 'onViewClicked'");
    view7f0a0071 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    DocumentActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.rvDocuments = null;
    target.tvNoDocument = null;

    view7f0a0071.setOnClickListener(null);
    view7f0a0071 = null;
  }
}
