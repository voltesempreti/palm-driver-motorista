// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.wallet_detail;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WalletDetailActivity_ViewBinding implements Unbinder {
  private WalletDetailActivity target;

  @UiThread
  public WalletDetailActivity_ViewBinding(WalletDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WalletDetailActivity_ViewBinding(WalletDetailActivity target, View source) {
    this.target = target;

    target.rvWalletDetailData = Utils.findRequiredViewAsType(source, R.id.rvWalletDetailData, "field 'rvWalletDetailData'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    WalletDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvWalletDetailData = null;
  }
}
