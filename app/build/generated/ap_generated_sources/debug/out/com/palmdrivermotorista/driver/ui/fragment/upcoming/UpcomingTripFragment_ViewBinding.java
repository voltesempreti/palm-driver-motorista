// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.fragment.upcoming;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UpcomingTripFragment_ViewBinding implements Unbinder {
  private UpcomingTripFragment target;

  @UiThread
  public UpcomingTripFragment_ViewBinding(UpcomingTripFragment target, View source) {
    this.target = target;

    target.upcomingTripRv = Utils.findRequiredViewAsType(source, R.id.upcoming_trip_rv, "field 'upcomingTripRv'", RecyclerView.class);
    target.errorLayout = Utils.findRequiredViewAsType(source, R.id.error_layout, "field 'errorLayout'", LinearLayout.class);
    target.error = Utils.findRequiredViewAsType(source, R.id.tv_error, "field 'error'", TextView.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progress_bar, "field 'progressBar'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    UpcomingTripFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.upcomingTripRv = null;
    target.errorLayout = null;
    target.error = null;
    target.progressBar = null;
  }
}
