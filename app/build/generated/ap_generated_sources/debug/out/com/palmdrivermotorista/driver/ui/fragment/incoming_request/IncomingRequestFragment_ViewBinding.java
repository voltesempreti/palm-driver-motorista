// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.fragment.incoming_request;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatRatingBar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class IncomingRequestFragment_ViewBinding implements Unbinder {
  private IncomingRequestFragment target;

  private View view7f0a007d;

  private View view7f0a0074;

  @UiThread
  public IncomingRequestFragment_ViewBinding(final IncomingRequestFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.btnReject, "field 'btnReject' and method 'onViewClicked'");
    target.btnReject = Utils.castView(view, R.id.btnReject, "field 'btnReject'", Button.class);
    view7f0a007d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btnAccept, "field 'btnAccept' and method 'onViewClicked'");
    target.btnAccept = Utils.castView(view, R.id.btnAccept, "field 'btnAccept'", Button.class);
    view7f0a0074 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.lblCount = Utils.findRequiredViewAsType(source, R.id.lblCount, "field 'lblCount'", TextView.class);
    target.imgUser = Utils.findRequiredViewAsType(source, R.id.imgUser, "field 'imgUser'", CircleImageView.class);
    target.lblUserName = Utils.findRequiredViewAsType(source, R.id.lblUserName, "field 'lblUserName'", TextView.class);
    target.ratingUser = Utils.findRequiredViewAsType(source, R.id.ratingUser, "field 'ratingUser'", AppCompatRatingBar.class);
    target.lblLocationName = Utils.findRequiredViewAsType(source, R.id.lblLocationName, "field 'lblLocationName'", TextView.class);
    target.DestLocationName = Utils.findRequiredViewAsType(source, R.id.DestLocationName, "field 'DestLocationName'", TextView.class);
    target.lblDrop = Utils.findRequiredViewAsType(source, R.id.lblDrop, "field 'lblDrop'", TextView.class);
    target.lblScheduleDate = Utils.findRequiredViewAsType(source, R.id.lblScheduleDate, "field 'lblScheduleDate'", TextView.class);
    target.Distance = Utils.findRequiredViewAsType(source, R.id.Distance, "field 'Distance'", TextView.class);
    target.PaymentType = Utils.findRequiredViewAsType(source, R.id.PaymentType, "field 'PaymentType'", TextView.class);
    target.DistanceUser = Utils.findRequiredViewAsType(source, R.id.DistanceUser, "field 'DistanceUser'", TextView.class);
    target.EstimatedFare = Utils.findRequiredViewAsType(source, R.id.EstimatedFare, "field 'EstimatedFare'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    IncomingRequestFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnReject = null;
    target.btnAccept = null;
    target.lblCount = null;
    target.imgUser = null;
    target.lblUserName = null;
    target.ratingUser = null;
    target.lblLocationName = null;
    target.DestLocationName = null;
    target.lblDrop = null;
    target.lblScheduleDate = null;
    target.Distance = null;
    target.PaymentType = null;
    target.DistanceUser = null;
    target.EstimatedFare = null;

    view7f0a007d.setOnClickListener(null);
    view7f0a007d = null;
    view7f0a0074.setOnClickListener(null);
    view7f0a0074 = null;
  }
}
