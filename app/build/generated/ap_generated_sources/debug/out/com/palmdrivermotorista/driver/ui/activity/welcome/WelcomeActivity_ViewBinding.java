// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.welcome;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WelcomeActivity_ViewBinding implements Unbinder {
  private WelcomeActivity target;

  private View view7f0a02a0;

  private View view7f0a02a1;

  private View view7f0a02ab;

  @UiThread
  public WelcomeActivity_ViewBinding(WelcomeActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WelcomeActivity_ViewBinding(final WelcomeActivity target, View source) {
    this.target = target;

    View view;
    target.viewPager = Utils.findRequiredViewAsType(source, R.id.view_pager, "field 'viewPager'", ViewPager.class);
    target.layoutDots = Utils.findRequiredViewAsType(source, R.id.layoutDots, "field 'layoutDots'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.sign_in, "field 'signIn' and method 'onViewClicked'");
    target.signIn = Utils.castView(view, R.id.sign_in, "field 'signIn'", Button.class);
    view7f0a02a0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sign_up, "field 'signUp' and method 'onViewClicked'");
    target.signUp = Utils.castView(view, R.id.sign_up, "field 'signUp'", Button.class);
    view7f0a02a1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.social_login, "field 'socialLogin' and method 'onViewClicked'");
    target.socialLogin = Utils.castView(view, R.id.social_login, "field 'socialLogin'", TextView.class);
    view7f0a02ab = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    WelcomeActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewPager = null;
    target.layoutDots = null;
    target.signIn = null;
    target.signUp = null;
    target.socialLogin = null;

    view7f0a02a0.setOnClickListener(null);
    view7f0a02a0 = null;
    view7f0a02a1.setOnClickListener(null);
    view7f0a02a1 = null;
    view7f0a02ab.setOnClickListener(null);
    view7f0a02ab = null;
  }
}
