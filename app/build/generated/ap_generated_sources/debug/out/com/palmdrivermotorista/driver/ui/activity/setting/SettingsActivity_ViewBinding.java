// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.setting;

import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SettingsActivity_ViewBinding implements Unbinder {
  private SettingsActivity target;

  private View view7f0a030f;

  @UiThread
  public SettingsActivity_ViewBinding(SettingsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SettingsActivity_ViewBinding(final SettingsActivity target, View source) {
    this.target = target;

    View view;
    target.english = Utils.findRequiredViewAsType(source, R.id.english, "field 'english'", RadioButton.class);
    target.arabic = Utils.findRequiredViewAsType(source, R.id.arabic, "field 'arabic'", RadioButton.class);
    target.chooseLanguage = Utils.findRequiredViewAsType(source, R.id.choose_language, "field 'chooseLanguage'", RadioGroup.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    view = Utils.findRequiredView(source, R.id.tvChangeDoc, "method 'onViewClicked'");
    view7f0a030f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SettingsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.english = null;
    target.arabic = null;
    target.chooseLanguage = null;
    target.toolbar = null;

    view7f0a030f.setOnClickListener(null);
    view7f0a030f = null;
  }
}
