// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.register;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RegisterActivity_ViewBinding implements Unbinder {
  private RegisterActivity target;

  private View view7f0a0063;

  private View view7f0a020f;

  private View view7f0a01a2;

  @UiThread
  public RegisterActivity_ViewBinding(RegisterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RegisterActivity_ViewBinding(final RegisterActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.back, "field 'back' and method 'onViewClicked'");
    target.back = Utils.castView(view, R.id.back, "field 'back'", ImageView.class);
    view7f0a0063 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.title = Utils.findRequiredViewAsType(source, R.id.title, "field 'title'", TextView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtEmail = Utils.findRequiredViewAsType(source, R.id.txtEmail, "field 'txtEmail'", EditText.class);
    target.txtFirstName = Utils.findRequiredViewAsType(source, R.id.txtFirstName, "field 'txtFirstName'", EditText.class);
    target.txtLastName = Utils.findRequiredViewAsType(source, R.id.txtLastName, "field 'txtLastName'", EditText.class);
    target.txtCpf = Utils.findRequiredViewAsType(source, R.id.txtCpf, "field 'txtCpf'", EditText.class);
    target.txtPassword = Utils.findRequiredViewAsType(source, R.id.txtPassword, "field 'txtPassword'", EditText.class);
    target.txtConfirmPassword = Utils.findRequiredViewAsType(source, R.id.txtConfirmPassword, "field 'txtConfirmPassword'", EditText.class);
    target.chkTerms = Utils.findRequiredViewAsType(source, R.id.chkTerms, "field 'chkTerms'", CheckBox.class);
    target.spinnerServiceType = Utils.findRequiredViewAsType(source, R.id.spinnerServiceType, "field 'spinnerServiceType'", AppCompatSpinner.class);
    target.txtVehicleModel = Utils.findRequiredViewAsType(source, R.id.txtVehicleModel, "field 'txtVehicleModel'", EditText.class);
    target.txtVehicleNumber = Utils.findRequiredViewAsType(source, R.id.txtVehicleNumber, "field 'txtVehicleNumber'", EditText.class);
    target.lnrReferralCode = Utils.findRequiredViewAsType(source, R.id.lnrReferralCode, "field 'lnrReferralCode'", LinearLayout.class);
    target.txtReferalCode = Utils.findRequiredViewAsType(source, R.id.txtReferalCode, "field 'txtReferalCode'", EditText.class);
    target.countryImage = Utils.findRequiredViewAsType(source, R.id.countryImage, "field 'countryImage'", ImageView.class);
    target.countryNumber = Utils.findRequiredViewAsType(source, R.id.countryNumber, "field 'countryNumber'", TextView.class);
    target.phoneNumber = Utils.findRequiredViewAsType(source, R.id.phoneNumber, "field 'phoneNumber'", EditText.class);
    target.state_Spinner = Utils.findRequiredViewAsType(source, R.id.state_spinner, "field 'state_Spinner'", AppCompatSpinner.class);
    target.city_Spinner = Utils.findRequiredViewAsType(source, R.id.city_spinner, "field 'city_Spinner'", AppCompatSpinner.class);
    target.sexy_Spinner = Utils.findRequiredViewAsType(source, R.id.sexy_spinner, "field 'sexy_Spinner'", AppCompatSpinner.class);
    view = Utils.findRequiredView(source, R.id.next, "method 'onViewClicked'");
    view7f0a020f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.lblTerms, "method 'onViewClicked'");
    view7f0a01a2 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RegisterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.back = null;
    target.title = null;
    target.toolbar = null;
    target.txtEmail = null;
    target.txtFirstName = null;
    target.txtLastName = null;
    target.txtCpf = null;
    target.txtPassword = null;
    target.txtConfirmPassword = null;
    target.chkTerms = null;
    target.spinnerServiceType = null;
    target.txtVehicleModel = null;
    target.txtVehicleNumber = null;
    target.lnrReferralCode = null;
    target.txtReferalCode = null;
    target.countryImage = null;
    target.countryNumber = null;
    target.phoneNumber = null;
    target.state_Spinner = null;
    target.city_Spinner = null;
    target.sexy_Spinner = null;

    view7f0a0063.setOnClickListener(null);
    view7f0a0063 = null;
    view7f0a020f.setOnClickListener(null);
    view7f0a020f = null;
    view7f0a01a2.setOnClickListener(null);
    view7f0a01a2 = null;
  }
}
