// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.invite_friend;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InviteFriendActivity_ViewBinding implements Unbinder {
  private InviteFriendActivity target;

  private View view7f0a0299;

  @UiThread
  public InviteFriendActivity_ViewBinding(InviteFriendActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public InviteFriendActivity_ViewBinding(final InviteFriendActivity target, View source) {
    this.target = target;

    View view;
    target.invite_friend = Utils.findRequiredViewAsType(source, R.id.invite_friend, "field 'invite_friend'", TextView.class);
    target.referral_code = Utils.findRequiredViewAsType(source, R.id.referral_code, "field 'referral_code'", TextView.class);
    target.referral_amount = Utils.findRequiredViewAsType(source, R.id.referral_amount, "field 'referral_amount'", TextView.class);
    view = Utils.findRequiredView(source, R.id.share, "method 'onClickAction'");
    view7f0a0299 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickAction(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    InviteFriendActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.invite_friend = null;
    target.referral_code = null;
    target.referral_amount = null;

    view7f0a0299.setOnClickListener(null);
    view7f0a0299 = null;
  }
}
