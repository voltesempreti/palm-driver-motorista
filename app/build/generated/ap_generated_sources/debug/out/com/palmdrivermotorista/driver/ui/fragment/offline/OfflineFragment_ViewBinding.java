// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.fragment.offline;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.gauravbhola.ripplepulsebackground.RipplePulseLayout;
import com.palmdrivermotorista.driver.R;
import ir.sohreco.circularpulsingbutton.CircularPulsingButton;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OfflineFragment_ViewBinding implements Unbinder {
  private OfflineFragment target;

  private View view7f0a01d0;

  private View view7f0a02cb;

  private View view7f0a007c;

  @UiThread
  public OfflineFragment_ViewBinding(final OfflineFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.menu_img, "field 'menuImg' and method 'onViewClicked'");
    target.menuImg = Utils.castView(view, R.id.menu_img, "field 'menuImg'", ImageView.class);
    view7f0a01d0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.tvApprovalDesc = Utils.findRequiredViewAsType(source, R.id.tvApprovalDesc, "field 'tvApprovalDesc'", TextView.class);
    view = Utils.findRequiredView(source, R.id.swipeBtnEnabled, "field 'swipeBtnEnabled' and method 'onViewClicked'");
    target.swipeBtnEnabled = Utils.castView(view, R.id.swipeBtnEnabled, "field 'swipeBtnEnabled'", CircularPulsingButton.class);
    view7f0a02cb = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btnPay, "field 'btnPay' and method 'onViewClicked'");
    target.btnPay = Utils.castView(view, R.id.btnPay, "field 'btnPay'", Button.class);
    view7f0a007c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutRipplepulse = Utils.findRequiredViewAsType(source, R.id.layout_ripplepulse, "field 'layoutRipplepulse'", RipplePulseLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OfflineFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.menuImg = null;
    target.tvApprovalDesc = null;
    target.swipeBtnEnabled = null;
    target.btnPay = null;
    target.layoutRipplepulse = null;

    view7f0a01d0.setOnClickListener(null);
    view7f0a01d0 = null;
    view7f0a02cb.setOnClickListener(null);
    view7f0a02cb = null;
    view7f0a007c.setOnClickListener(null);
    view7f0a007c = null;
  }
}
