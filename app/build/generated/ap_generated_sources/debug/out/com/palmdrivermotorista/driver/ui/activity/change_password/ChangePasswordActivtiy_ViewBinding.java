// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.change_password;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChangePasswordActivtiy_ViewBinding implements Unbinder {
  private ChangePasswordActivtiy target;

  private View view7f0a0077;

  @UiThread
  public ChangePasswordActivtiy_ViewBinding(ChangePasswordActivtiy target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ChangePasswordActivtiy_ViewBinding(final ChangePasswordActivtiy target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtCurrentPassword = Utils.findRequiredViewAsType(source, R.id.txtCurrentPassword, "field 'txtCurrentPassword'", EditText.class);
    target.txtNewPassword = Utils.findRequiredViewAsType(source, R.id.txtNewPassword, "field 'txtNewPassword'", EditText.class);
    target.txtConfirmPassword = Utils.findRequiredViewAsType(source, R.id.txtConfirmPassword, "field 'txtConfirmPassword'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btnChangePassword, "field 'btnChangePassword' and method 'onViewClicked'");
    target.btnChangePassword = Utils.castView(view, R.id.btnChangePassword, "field 'btnChangePassword'", Button.class);
    view7f0a0077 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ChangePasswordActivtiy target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtCurrentPassword = null;
    target.txtNewPassword = null;
    target.txtConfirmPassword = null;
    target.btnChangePassword = null;

    view7f0a0077.setOnClickListener(null);
    view7f0a0077 = null;
  }
}
