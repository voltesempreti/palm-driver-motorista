// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.reset_password;

import android.view.View;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ResetActivity_ViewBinding implements Unbinder {
  private ResetActivity target;

  private View view7f0a0063;

  private View view7f0a007a;

  @UiThread
  public ResetActivity_ViewBinding(ResetActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ResetActivity_ViewBinding(final ResetActivity target, View source) {
    this.target = target;

    View view;
    target.txtOTP = Utils.findRequiredViewAsType(source, R.id.txtOTP, "field 'txtOTP'", EditText.class);
    target.txtNewPassword = Utils.findRequiredViewAsType(source, R.id.txtNewPassword, "field 'txtNewPassword'", EditText.class);
    target.txtPassword = Utils.findRequiredViewAsType(source, R.id.txtPassword, "field 'txtPassword'", EditText.class);
    view = Utils.findRequiredView(source, R.id.back, "method 'onViewClicked'");
    view7f0a0063 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btnDone, "method 'onViewClicked'");
    view7f0a007a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ResetActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtOTP = null;
    target.txtNewPassword = null;
    target.txtPassword = null;

    view7f0a0063.setOnClickListener(null);
    view7f0a0063 = null;
    view7f0a007a.setOnClickListener(null);
    view7f0a007a = null;
  }
}
