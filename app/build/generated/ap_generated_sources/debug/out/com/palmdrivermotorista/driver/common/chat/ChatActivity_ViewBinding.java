// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.common.chat;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChatActivity_ViewBinding implements Unbinder {
  private ChatActivity target;

  private View view7f0a0295;

  @UiThread
  public ChatActivity_ViewBinding(ChatActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ChatActivity_ViewBinding(final ChatActivity target, View source) {
    this.target = target;

    View view;
    target.chatLv = Utils.findRequiredViewAsType(source, R.id.chat_lv, "field 'chatLv'", ListView.class);
    target.message = Utils.findRequiredViewAsType(source, R.id.message, "field 'message'", EditText.class);
    view = Utils.findRequiredView(source, R.id.send, "field 'send' and method 'onViewClicked'");
    target.send = Utils.castView(view, R.id.send, "field 'send'", ImageView.class);
    view7f0a0295 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.chatControlsLayout = Utils.findRequiredViewAsType(source, R.id.chat_controls_layout, "field 'chatControlsLayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ChatActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.chatLv = null;
    target.message = null;
    target.send = null;
    target.chatControlsLayout = null;

    view7f0a0295.setOnClickListener(null);
    view7f0a0295 = null;
  }
}
