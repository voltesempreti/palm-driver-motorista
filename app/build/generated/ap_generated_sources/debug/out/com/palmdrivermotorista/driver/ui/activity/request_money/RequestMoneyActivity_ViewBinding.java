// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.request_money;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RequestMoneyActivity_ViewBinding implements Unbinder {
  private RequestMoneyActivity target;

  private View view7f0a0320;

  @UiThread
  public RequestMoneyActivity_ViewBinding(RequestMoneyActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RequestMoneyActivity_ViewBinding(final RequestMoneyActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.etRequestAmt = Utils.findRequiredViewAsType(source, R.id.etRequestAmt, "field 'etRequestAmt'", EditText.class);
    target.rvRequestedData = Utils.findRequiredViewAsType(source, R.id.rvRequestedData, "field 'rvRequestedData'", RecyclerView.class);
    target.llHistoryContainer = Utils.findRequiredViewAsType(source, R.id.llHistoryContainer, "field 'llHistoryContainer'", LinearLayout.class);
    target.tvHistoryPlaceholder = Utils.findRequiredViewAsType(source, R.id.tvHistoryPlaceholder, "field 'tvHistoryPlaceholder'", TextView.class);
    view = Utils.findRequiredView(source, R.id.tvSubmit, "method 'onViewClicked'");
    view7f0a0320 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RequestMoneyActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.etRequestAmt = null;
    target.rvRequestedData = null;
    target.llHistoryContainer = null;
    target.tvHistoryPlaceholder = null;

    view7f0a0320.setOnClickListener(null);
    view7f0a0320 = null;
  }
}
