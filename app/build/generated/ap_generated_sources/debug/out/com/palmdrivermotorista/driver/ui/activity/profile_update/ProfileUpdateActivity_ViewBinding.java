// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.profile_update;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfileUpdateActivity_ViewBinding implements Unbinder {
  private ProfileUpdateActivity target;

  private View view7f0a0169;

  private View view7f0a0336;

  private View view7f0a007e;

  private View view7f0a018d;

  private View view7f0a0259;

  @UiThread
  public ProfileUpdateActivity_ViewBinding(ProfileUpdateActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProfileUpdateActivity_ViewBinding(final ProfileUpdateActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.imgProfile, "field 'imgProfile' and method 'onViewClicked'");
    target.imgProfile = Utils.castView(view, R.id.imgProfile, "field 'imgProfile'", CircleImageView.class);
    view7f0a0169 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.completeData = Utils.findRequiredViewAsType(source, R.id.completeData, "field 'completeData'", LinearLayout.class);
    target.txtFirstName = Utils.findRequiredViewAsType(source, R.id.txtFirstName, "field 'txtFirstName'", EditText.class);
    target.txtLastName = Utils.findRequiredViewAsType(source, R.id.txtLastName, "field 'txtLastName'", EditText.class);
    target.txtCpf = Utils.findRequiredViewAsType(source, R.id.txtCpf, "field 'txtCpf'", EditText.class);
    view = Utils.findRequiredView(source, R.id.txtPhoneNumber, "field 'txtPhoneNumber' and method 'onViewClicked'");
    target.txtPhoneNumber = Utils.castView(view, R.id.txtPhoneNumber, "field 'txtPhoneNumber'", EditText.class);
    view7f0a0336 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtEmail = Utils.findRequiredViewAsType(source, R.id.txtEmail, "field 'txtEmail'", EditText.class);
    target.txtService = Utils.findRequiredViewAsType(source, R.id.txtService, "field 'txtService'", EditText.class);
    target.txtModel = Utils.findRequiredViewAsType(source, R.id.txtModel, "field 'txtModel'", EditText.class);
    target.txtNumber = Utils.findRequiredViewAsType(source, R.id.txtNumber, "field 'txtNumber'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btnSave, "field 'btnSave' and method 'onViewClicked'");
    target.btnSave = Utils.castView(view, R.id.btnSave, "field 'btnSave'", Button.class);
    view7f0a007e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.lblChangePassword, "field 'lblChangePassword' and method 'onViewClicked'");
    target.lblChangePassword = Utils.castView(view, R.id.lblChangePassword, "field 'lblChangePassword'", TextView.class);
    view7f0a018d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.qr_scan, "field 'ivQrScan' and method 'onViewClicked'");
    target.ivQrScan = Utils.castView(view, R.id.qr_scan, "field 'ivQrScan'", ImageView.class);
    view7f0a0259 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ProfileUpdateActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgProfile = null;
    target.completeData = null;
    target.txtFirstName = null;
    target.txtLastName = null;
    target.txtCpf = null;
    target.txtPhoneNumber = null;
    target.txtEmail = null;
    target.txtService = null;
    target.txtModel = null;
    target.txtNumber = null;
    target.btnSave = null;
    target.lblChangePassword = null;
    target.ivQrScan = null;

    view7f0a0169.setOnClickListener(null);
    view7f0a0169 = null;
    view7f0a0336.setOnClickListener(null);
    view7f0a0336 = null;
    view7f0a007e.setOnClickListener(null);
    view7f0a007e = null;
    view7f0a018d.setOnClickListener(null);
    view7f0a018d = null;
    view7f0a0259.setOnClickListener(null);
    view7f0a0259 = null;
  }
}
