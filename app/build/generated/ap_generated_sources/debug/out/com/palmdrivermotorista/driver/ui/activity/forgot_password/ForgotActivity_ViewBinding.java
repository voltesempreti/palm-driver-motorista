// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.forgot_password;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ForgotActivity_ViewBinding implements Unbinder {
  private ForgotActivity target;

  private View view7f0a0063;

  private View view7f0a020f;

  @UiThread
  public ForgotActivity_ViewBinding(ForgotActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ForgotActivity_ViewBinding(final ForgotActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.back, "field 'back' and method 'onViewClicked'");
    target.back = Utils.castView(view, R.id.back, "field 'back'", ImageView.class);
    view7f0a0063 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtEmail = Utils.findRequiredViewAsType(source, R.id.txtEmail, "field 'txtEmail'", EditText.class);
    view = Utils.findRequiredView(source, R.id.next, "field 'next' and method 'onViewClicked'");
    target.next = Utils.castView(view, R.id.next, "field 'next'", FloatingActionButton.class);
    view7f0a020f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ForgotActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.back = null;
    target.txtEmail = null;
    target.next = null;

    view7f0a0063.setOnClickListener(null);
    view7f0a0063 = null;
    view7f0a020f.setOnClickListener(null);
    view7f0a020f = null;
  }
}
