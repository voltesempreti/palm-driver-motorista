// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.past_detail;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PastTripDetailActivity_ViewBinding implements Unbinder {
  private PastTripDetailActivity target;

  private View view7f0a034b;

  @UiThread
  public PastTripDetailActivity_ViewBinding(PastTripDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PastTripDetailActivity_ViewBinding(final PastTripDetailActivity target, View source) {
    this.target = target;

    View view;
    target.staticMap = Utils.findRequiredViewAsType(source, R.id.static_map, "field 'staticMap'", ImageView.class);
    target.avatar = Utils.findRequiredViewAsType(source, R.id.avatar, "field 'avatar'", CircleImageView.class);
    target.firstName = Utils.findRequiredViewAsType(source, R.id.first_name, "field 'firstName'", TextView.class);
    target.ratingBar = Utils.findRequiredViewAsType(source, R.id.rating, "field 'ratingBar'", AppCompatRatingBar.class);
    target.finishedAt = Utils.findRequiredViewAsType(source, R.id.finished_at, "field 'finishedAt'", TextView.class);
    target.bookingId = Utils.findRequiredViewAsType(source, R.id.booking_id, "field 'bookingId'", TextView.class);
    target.paymentMode = Utils.findRequiredViewAsType(source, R.id.payment_mode, "field 'paymentMode'", TextView.class);
    target.payable = Utils.findRequiredViewAsType(source, R.id.payable, "field 'payable'", TextView.class);
    target.userComment = Utils.findRequiredViewAsType(source, R.id.user_comment, "field 'userComment'", TextView.class);
    view = Utils.findRequiredView(source, R.id.view_receipt, "field 'viewReceipt' and method 'onViewClicked'");
    target.viewReceipt = Utils.castView(view, R.id.view_receipt, "field 'viewReceipt'", Button.class);
    view7f0a034b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.lblSource = Utils.findRequiredViewAsType(source, R.id.lblSource, "field 'lblSource'", TextView.class);
    target.lblDestination = Utils.findRequiredViewAsType(source, R.id.lblDestination, "field 'lblDestination'", TextView.class);
    target.paymentImage = Utils.findRequiredViewAsType(source, R.id.payment_image, "field 'paymentImage'", ImageView.class);
    target.finishedAtTime = Utils.findRequiredViewAsType(source, R.id.finished_at_time, "field 'finishedAtTime'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PastTripDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.staticMap = null;
    target.avatar = null;
    target.firstName = null;
    target.ratingBar = null;
    target.finishedAt = null;
    target.bookingId = null;
    target.paymentMode = null;
    target.payable = null;
    target.userComment = null;
    target.viewReceipt = null;
    target.toolbar = null;
    target.lblSource = null;
    target.lblDestination = null;
    target.paymentImage = null;
    target.finishedAtTime = null;

    view7f0a034b.setOnClickListener(null);
    view7f0a034b = null;
  }
}
