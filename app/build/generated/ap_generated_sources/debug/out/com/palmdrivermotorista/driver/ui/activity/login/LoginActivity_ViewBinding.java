// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.login;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding implements Unbinder {
  private LoginActivity target;

  private View view7f0a0073;

  private View view7f0a0147;

  private View view7f0a02a1;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(final LoginActivity target, View source) {
    this.target = target;

    View view;
    target.email = Utils.findRequiredViewAsType(source, R.id.login_email, "field 'email'", EditText.class);
    target.password = Utils.findRequiredViewAsType(source, R.id.login_password, "field 'password'", EditText.class);
    view = Utils.findRequiredView(source, R.id.bt_login, "field 'login' and method 'onViewClicked'");
    target.login = Utils.castView(view, R.id.bt_login, "field 'login'", Button.class);
    view7f0a0073 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mToolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'mToolbar'", Toolbar.class);
    view = Utils.findRequiredView(source, R.id.forgot_password, "method 'onViewClicked'");
    view7f0a0147 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sign_up, "method 'onViewClicked'");
    view7f0a02a1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.email = null;
    target.password = null;
    target.login = null;
    target.mToolbar = null;

    view7f0a0073.setOnClickListener(null);
    view7f0a0073 = null;
    view7f0a0147.setOnClickListener(null);
    view7f0a0147 = null;
    view7f0a02a1.setOnClickListener(null);
    view7f0a02a1 = null;
  }
}
