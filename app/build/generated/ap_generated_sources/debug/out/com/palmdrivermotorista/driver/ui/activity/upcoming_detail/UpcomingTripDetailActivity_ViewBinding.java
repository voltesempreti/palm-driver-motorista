// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.upcoming_detail;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UpcomingTripDetailActivity_ViewBinding implements Unbinder {
  private UpcomingTripDetailActivity target;

  private View view7f0a0086;

  private View view7f0a0085;

  @UiThread
  public UpcomingTripDetailActivity_ViewBinding(UpcomingTripDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public UpcomingTripDetailActivity_ViewBinding(final UpcomingTripDetailActivity target,
      View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    view = Utils.findRequiredView(source, R.id.cancel, "field 'cancel' and method 'onViewClicked'");
    target.cancel = Utils.castView(view, R.id.cancel, "field 'cancel'", Button.class);
    view7f0a0086 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.call, "field 'call' and method 'onViewClicked'");
    target.call = Utils.castView(view, R.id.call, "field 'call'", Button.class);
    view7f0a0085 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.staticMap = Utils.findRequiredViewAsType(source, R.id.static_map, "field 'staticMap'", ImageView.class);
    target.avatar = Utils.findRequiredViewAsType(source, R.id.avatar, "field 'avatar'", CircleImageView.class);
    target.firstName = Utils.findRequiredViewAsType(source, R.id.first_name, "field 'firstName'", TextView.class);
    target.rating = Utils.findRequiredViewAsType(source, R.id.rating, "field 'rating'", AppCompatRatingBar.class);
    target.bookingId = Utils.findRequiredViewAsType(source, R.id.booking_id, "field 'bookingId'", TextView.class);
    target.scheduleAt = Utils.findRequiredViewAsType(source, R.id.schedule_at, "field 'scheduleAt'", TextView.class);
    target.lblSource = Utils.findRequiredViewAsType(source, R.id.lblSource, "field 'lblSource'", TextView.class);
    target.lblDestination = Utils.findRequiredViewAsType(source, R.id.lblDestination, "field 'lblDestination'", TextView.class);
    target.paymentMode = Utils.findRequiredViewAsType(source, R.id.payment_mode, "field 'paymentMode'", TextView.class);
    target.payable = Utils.findRequiredViewAsType(source, R.id.payable, "field 'payable'", TextView.class);
    target.upcomingPayment = Utils.findRequiredViewAsType(source, R.id.upcoming_payment, "field 'upcomingPayment'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    UpcomingTripDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.cancel = null;
    target.call = null;
    target.staticMap = null;
    target.avatar = null;
    target.firstName = null;
    target.rating = null;
    target.bookingId = null;
    target.scheduleAt = null;
    target.lblSource = null;
    target.lblDestination = null;
    target.paymentMode = null;
    target.payable = null;
    target.upcomingPayment = null;

    view7f0a0086.setOnClickListener(null);
    view7f0a0086 = null;
    view7f0a0085.setOnClickListener(null);
    view7f0a0085 = null;
  }
}
