// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.bottomsheetdialog.invoice_flow;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InvoiceDialogFragment_ViewBinding implements Unbinder {
  private InvoiceDialogFragment target;

  private View view7f0a0079;

  private View view7f0a007b;

  @UiThread
  public InvoiceDialogFragment_ViewBinding(final InvoiceDialogFragment target, View source) {
    this.target = target;

    View view;
    target.promotionAmount = Utils.findRequiredViewAsType(source, R.id.promotion_amount, "field 'promotionAmount'", TextView.class);
    target.walletAmount = Utils.findRequiredViewAsType(source, R.id.wallet_amount, "field 'walletAmount'", TextView.class);
    target.bookingId = Utils.findRequiredViewAsType(source, R.id.booking_id, "field 'bookingId'", TextView.class);
    target.payableAmount = Utils.findRequiredViewAsType(source, R.id.payable_amount, "field 'payableAmount'", TextView.class);
    target.paymentModeImg = Utils.findRequiredViewAsType(source, R.id.payment_mode_img, "field 'paymentModeImg'", ImageView.class);
    target.paymentModeLayout = Utils.findRequiredViewAsType(source, R.id.payment_mode_layout, "field 'paymentModeLayout'", LinearLayout.class);
    target.llAmountToBePaid = Utils.findRequiredViewAsType(source, R.id.llAmountToBePaid, "field 'llAmountToBePaid'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.btnConfirmPayment, "field 'btnConfirmPayment' and method 'onViewClicked'");
    target.btnConfirmPayment = Utils.castView(view, R.id.btnConfirmPayment, "field 'btnConfirmPayment'", Button.class);
    view7f0a0079 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btnEnableVirtualChange, "field 'btnEnableVirtualChange' and method 'onViewClicked'");
    target.btnEnableVirtualChange = Utils.castView(view, R.id.btnEnableVirtualChange, "field 'btnEnableVirtualChange'", Button.class);
    view7f0a007b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.lnrVirtualChange = Utils.findRequiredViewAsType(source, R.id.lnrVirtualChange, "field 'lnrVirtualChange'", LinearLayout.class);
    target.changeValue = Utils.findRequiredViewAsType(source, R.id.change_value, "field 'changeValue'", EditText.class);
    target.lblPaymentType = Utils.findRequiredViewAsType(source, R.id.lblPaymentType, "field 'lblPaymentType'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    InvoiceDialogFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.promotionAmount = null;
    target.walletAmount = null;
    target.bookingId = null;
    target.payableAmount = null;
    target.paymentModeImg = null;
    target.paymentModeLayout = null;
    target.llAmountToBePaid = null;
    target.btnConfirmPayment = null;
    target.btnEnableVirtualChange = null;
    target.lnrVirtualChange = null;
    target.changeValue = null;
    target.lblPaymentType = null;

    view7f0a0079.setOnClickListener(null);
    view7f0a0079 = null;
    view7f0a007b.setOnClickListener(null);
    view7f0a007b = null;
  }
}
