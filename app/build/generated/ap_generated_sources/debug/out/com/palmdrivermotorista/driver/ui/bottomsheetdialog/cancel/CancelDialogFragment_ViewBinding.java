// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.bottomsheetdialog.cancel;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CancelDialogFragment_ViewBinding implements Unbinder {
  private CancelDialogFragment target;

  private View view7f0a0080;

  @UiThread
  public CancelDialogFragment_ViewBinding(final CancelDialogFragment target, View source) {
    this.target = target;

    View view;
    target.comments = Utils.findRequiredViewAsType(source, R.id.txtComments, "field 'comments'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btnSubmit, "field 'btnSubmit' and method 'onViewClicked'");
    target.btnSubmit = Utils.castView(view, R.id.btnSubmit, "field 'btnSubmit'", Button.class);
    view7f0a0080 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.rcvReason = Utils.findRequiredViewAsType(source, R.id.rcvReason, "field 'rcvReason'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CancelDialogFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.comments = null;
    target.btnSubmit = null;
    target.rcvReason = null;

    view7f0a0080.setOnClickListener(null);
    view7f0a0080 = null;
  }
}
