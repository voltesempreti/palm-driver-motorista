// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.card;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CardActivity_ViewBinding implements Unbinder {
  private CardActivity target;

  private View view7f0a004b;

  @UiThread
  public CardActivity_ViewBinding(CardActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CardActivity_ViewBinding(final CardActivity target, View source) {
    this.target = target;

    View view;
    target.cardsRv = Utils.findRequiredViewAsType(source, R.id.cards_rv, "field 'cardsRv'", RecyclerView.class);
    target.llCardContainer = Utils.findRequiredViewAsType(source, R.id.llCardContainer, "field 'llCardContainer'", LinearLayout.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    view = Utils.findRequiredView(source, R.id.addCard, "field 'addCard' and method 'onViewClicked'");
    target.addCard = Utils.castView(view, R.id.addCard, "field 'addCard'", Button.class);
    view7f0a004b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CardActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cardsRv = null;
    target.llCardContainer = null;
    target.toolbar = null;
    target.addCard = null;

    view7f0a004b.setOnClickListener(null);
    view7f0a004b = null;
  }
}
