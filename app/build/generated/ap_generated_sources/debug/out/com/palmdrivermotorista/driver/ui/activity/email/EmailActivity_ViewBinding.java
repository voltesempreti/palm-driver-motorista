// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.email;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmailActivity_ViewBinding implements Unbinder {
  private EmailActivity target;

  private View view7f0a02a1;

  private View view7f0a020f;

  private View view7f0a0063;

  @UiThread
  public EmailActivity_ViewBinding(EmailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public EmailActivity_ViewBinding(final EmailActivity target, View source) {
    this.target = target;

    View view;
    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", TextInputEditText.class);
    view = Utils.findRequiredView(source, R.id.sign_up, "field 'signUp' and method 'onViewClicked'");
    target.signUp = Utils.castView(view, R.id.sign_up, "field 'signUp'", TextView.class);
    view7f0a02a1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.next, "field 'next' and method 'onViewClicked'");
    target.next = Utils.castView(view, R.id.next, "field 'next'", FloatingActionButton.class);
    view7f0a020f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.back, "method 'onViewClicked'");
    view7f0a0063 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EmailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.email = null;
    target.signUp = null;
    target.next = null;

    view7f0a02a1.setOnClickListener(null);
    view7f0a02a1 = null;
    view7f0a020f.setOnClickListener(null);
    view7f0a020f = null;
    view7f0a0063.setOnClickListener(null);
    view7f0a0063 = null;
  }
}
