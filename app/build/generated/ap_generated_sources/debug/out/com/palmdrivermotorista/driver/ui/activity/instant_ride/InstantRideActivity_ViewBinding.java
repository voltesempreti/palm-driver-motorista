// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.instant_ride;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InstantRideActivity_ViewBinding implements Unbinder {
  private InstantRideActivity target;

  @UiThread
  public InstantRideActivity_ViewBinding(InstantRideActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public InstantRideActivity_ViewBinding(InstantRideActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.etDestination = Utils.findRequiredViewAsType(source, R.id.etDestination, "field 'etDestination'", EditText.class);
    target.llPhoneNumberContainer = Utils.findRequiredViewAsType(source, R.id.llPhoneNumberContainer, "field 'llPhoneNumberContainer'", LinearLayout.class);
    target.etPhoneNumber = Utils.findRequiredViewAsType(source, R.id.etPhoneNumber, "field 'etPhoneNumber'", EditText.class);
    target.rvLocation = Utils.findRequiredViewAsType(source, R.id.rvLocation, "field 'rvLocation'", RecyclerView.class);
    target.cvLocationsContainer = Utils.findRequiredViewAsType(source, R.id.cvLocationsContainer, "field 'cvLocationsContainer'", CardView.class);
    target.countryImage = Utils.findRequiredViewAsType(source, R.id.countryImage, "field 'countryImage'", ImageView.class);
    target.tvCountryCode = Utils.findRequiredViewAsType(source, R.id.countryNumber, "field 'tvCountryCode'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    InstantRideActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.etDestination = null;
    target.llPhoneNumberContainer = null;
    target.etPhoneNumber = null;
    target.rvLocation = null;
    target.cvLocationsContainer = null;
    target.countryImage = null;
    target.tvCountryCode = null;
  }
}
