// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.taximeter;

import android.view.View;
import android.widget.Button;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TaximeterActivity_ViewBinding implements Unbinder {
  private TaximeterActivity target;

  @UiThread
  public TaximeterActivity_ViewBinding(TaximeterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public TaximeterActivity_ViewBinding(TaximeterActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.start = Utils.findRequiredViewAsType(source, R.id.start_button, "field 'start'", Button.class);
    target.stop = Utils.findRequiredViewAsType(source, R.id.stop_button, "field 'stop'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    TaximeterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.start = null;
    target.stop = null;
  }
}
