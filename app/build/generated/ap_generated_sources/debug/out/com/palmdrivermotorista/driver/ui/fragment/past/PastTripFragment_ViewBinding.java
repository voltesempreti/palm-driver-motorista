// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.fragment.past;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PastTripFragment_ViewBinding implements Unbinder {
  private PastTripFragment target;

  @UiThread
  public PastTripFragment_ViewBinding(PastTripFragment target, View source) {
    this.target = target;

    target.pastTripRv = Utils.findRequiredViewAsType(source, R.id.past_trip_rv, "field 'pastTripRv'", RecyclerView.class);
    target.errorLayout = Utils.findRequiredViewAsType(source, R.id.error_layout, "field 'errorLayout'", LinearLayout.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progress_bar, "field 'progressBar'", ProgressBar.class);
    target.error = Utils.findRequiredViewAsType(source, R.id.tv_error, "field 'error'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PastTripFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.pastTripRv = null;
    target.errorLayout = null;
    target.progressBar = null;
    target.error = null;
  }
}
