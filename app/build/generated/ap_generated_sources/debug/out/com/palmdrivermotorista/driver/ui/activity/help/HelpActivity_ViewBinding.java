// Generated code from Butter Knife. Do not modify!
package com.palmdrivermotorista.driver.ui.activity.help;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdrivermotorista.driver.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HelpActivity_ViewBinding implements Unbinder {
  private HelpActivity target;

  private View view7f0a0164;

  private View view7f0a0165;

  private View view7f0a016c;

  @UiThread
  public HelpActivity_ViewBinding(HelpActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public HelpActivity_ViewBinding(final HelpActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    view = Utils.findRequiredView(source, R.id.imgCall, "field 'imgCall' and method 'onViewClicked'");
    target.imgCall = Utils.castView(view, R.id.imgCall, "field 'imgCall'", ImageView.class);
    view7f0a0164 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.imgMail, "field 'imgMail' and method 'onViewClicked'");
    target.imgMail = Utils.castView(view, R.id.imgMail, "field 'imgMail'", ImageView.class);
    view7f0a0165 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.imgWeb, "field 'imgWeb' and method 'onViewClicked'");
    target.imgWeb = Utils.castView(view, R.id.imgWeb, "field 'imgWeb'", ImageView.class);
    view7f0a016c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    HelpActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.imgCall = null;
    target.imgMail = null;
    target.imgWeb = null;

    view7f0a0164.setOnClickListener(null);
    view7f0a0164 = null;
    view7f0a0165.setOnClickListener(null);
    view7f0a0165 = null;
    view7f0a016c.setOnClickListener(null);
    view7f0a016c = null;
  }
}
