package com.palmdrivermotorista.driver.common.swipe_button;

public interface OnActiveListener {
    void onActive();
}
