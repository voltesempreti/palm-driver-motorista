package com.palmdrivermotorista.driver.ui.activity.add_card;

import com.palmdrivermotorista.driver.base.MvpView;

public interface AddCardIView extends MvpView {

    void onSuccess(Object card);

    void onError(Throwable e);
}
