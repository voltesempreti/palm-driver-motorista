package com.palmdrivermotorista.driver.ui.activity.wallet;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.palmdrivermotorista.driver.R;
import com.palmdrivermotorista.driver.base.BaseActivity;
import com.palmdrivermotorista.driver.common.Constants;
import com.palmdrivermotorista.driver.common.SharedHelper;
import com.palmdrivermotorista.driver.common.mask.MoneyTextWatcher;
import com.palmdrivermotorista.driver.data.network.model.WalletMoneyAddedResponse;
import com.palmdrivermotorista.driver.data.network.model.WalletResponse;
import com.palmdrivermotorista.driver.ui.activity.payment.PaymentActivity;
import com.palmdrivermotorista.driver.ui.activity.request_money.RequestMoneyActivity;
import com.palmdrivermotorista.driver.ui.adapter.WalletAdapter;

import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.palmdrivermotorista.driver.ui.activity.payment.PaymentActivity.PICK_PAYMENT_METHOD;

public class WalletActivity extends BaseActivity implements WalletIView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvWalletAmt)
    TextView tvWalletAmt;
    @BindView(R.id.rvWalletData)
    RecyclerView rvWalletData;
    @BindView(R.id.tvWalletPlaceholder)
    TextView tvWalletPlaceholder;
    @BindView(R.id.llWalletHistory)
    LinearLayout llWalletHistory;
    @BindView(R.id.ivRequestMoney)
    TextView ivRequestMoney;
    @BindView(R.id.addAmount)
    Button addAmount;
    @BindView(R.id.etRequestAmt)
    EditText etRequestAmt;
    private WalletPresenter mPresenter = new WalletPresenter();
    private double walletAmt;

    @Override
    public int getLayoutId() {
        return R.layout.activity_wallet;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        etRequestAmt.setKeyListener(new DigitsInputFilter());
        Locale mLocale = new Locale("pt", "BR");
        etRequestAmt.addTextChangedListener(new MoneyTextWatcher(etRequestAmt, mLocale));
        mPresenter.attachView(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.wallet));
        showLoading();
        if (SharedHelper.getIntKey(this, "card") == 0) ivRequestMoney.setVisibility(View.GONE);
        else ivRequestMoney.setVisibility(View.VISIBLE);
        mPresenter.getWalletData();
        rvWalletData.setLayoutManager(new LinearLayoutManager(activity(), LinearLayoutManager.VERTICAL, false));
        rvWalletData.setItemAnimator(new DefaultItemAnimator());
        rvWalletData.setHasFixedSize(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.addAmount)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addAmount:
                if (validateAmount()) {
                    Intent intent = new Intent(activity(), PaymentActivity.class);
                    intent.putExtra("hideCash", true);
                    startActivityForResult(intent, PICK_PAYMENT_METHOD);
                }
                break;

            default:
                break;
        }
    }

    public boolean validateAmount(){
        if(etRequestAmt.getText().toString().equals("")){
            Toast.makeText(this, "Preencha o valor do saldo antes de adicionar!", Toast.LENGTH_SHORT).show();
            return false;
        }


        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onSuccess(WalletResponse response) {
        hideLoading();
        walletAmt = response.getWalletBalance();
        if(walletAmt < 0){
            tvWalletAmt.setTextColor(ContextCompat.getColor(this, R.color.red));
        } else if(walletAmt > 0){
            tvWalletAmt.setTextColor(ContextCompat.getColor(this, R.color.green));
        }
        tvWalletAmt.setText(String.format("%s%s", Constants.Currency, walletAmt));
        if (response.getWalletTransation() != null && response.getWalletTransation().size() > 0) {
            rvWalletData.setAdapter(new WalletAdapter(response.getWalletTransation()));
            llWalletHistory.setVisibility(View.VISIBLE);
            tvWalletPlaceholder.setVisibility(View.GONE);
        } else {
            llWalletHistory.setVisibility(View.GONE);
            tvWalletPlaceholder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSuccess(WalletMoneyAddedResponse response) {
        showLoading();
        mPresenter.attachView(this);
        if (SharedHelper.getIntKey(this, "card") == 0) ivRequestMoney.setVisibility(View.GONE);
        else ivRequestMoney.setVisibility(View.VISIBLE);
        mPresenter.getWalletData();
        rvWalletData.setLayoutManager(new LinearLayoutManager(activity(), LinearLayoutManager.VERTICAL, false));
        rvWalletData.setItemAnimator(new DefaultItemAnimator());
        rvWalletData.setHasFixedSize(true);
        Toast.makeText(this, "Saldo adicionado com sucesso!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(Throwable e) {
        hideLoading();
        if (e != null)
            onErrorBase(e);
    }

    @OnClick(R.id.ivRequestMoney)
    public void onViewClicked() {
        startActivity(new Intent(this, RequestMoneyActivity.class).putExtra("WalletAmt", walletAmt));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PAYMENT_METHOD && resultCode == Activity.RESULT_OK && data != null)
            if (data.getStringExtra("payment_mode").equals("CARD")) {
                HashMap<String, Object> map = new HashMap<>();
                String cardId = data.getStringExtra("card_id");
                String stringValue = etRequestAmt.getText().toString().replaceAll("R", "").replaceAll("\\$", "").replaceAll("\\s+", "").replaceAll(",", ".");
                double value = Double.parseDouble(stringValue);
                map.put("amount", value);
                map.put("card_id", cardId);
                map.put("payment_mode", "CARD");
                map.put("user_type", "provider");
                showLoading();
                mPresenter.addMoney(map);
            }
    }

    public class DigitsInputFilter extends DigitsKeyListener {

        private int decimalPlaces = 2;

        DigitsInputFilter() {
            super(false, true);
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest,
                                   int dstart, int dend) {
            CharSequence out = super.filter(source, start, end, dest, dstart, dend);
            if (out != null) {
                source = out;
                start = 0;
                end = out.length();
            }

            int length = end - start;
            if (length == 0)
                return source;

            if (dend == 0 && source.toString().equals("."))
                return "";

            int destLength = dest.length();
            for (int i = 0; i < dstart; i++) {
                if (dest.charAt(i) == '.')
                    return (destLength - (i + 1) + length > decimalPlaces) ?
                            "" : new SpannableStringBuilder(source, start, end);
            }

            for (int i = start; i < end; ++i) {
                if (source.charAt(i) == '.') {
                    if ((destLength - dend) + (end - (i + 1)) > decimalPlaces)
                        return "";
                    else
                        break;
                }
            }

            return new SpannableStringBuilder(source, start, end);
        }
    }
}
