package com.palmdrivermotorista.driver.ui.activity.wallet_detail;

import com.palmdrivermotorista.driver.base.MvpPresenter;
import com.palmdrivermotorista.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIPresenter<V extends WalletDetailIView> extends MvpPresenter<V> {
    void setAdapter(ArrayList<Transaction> myList);
}
