package com.palmdrivermotorista.driver.ui.activity.help;


import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface HelpIPresenter<V extends HelpIView> extends MvpPresenter<V> {

    void getHelp();
}
