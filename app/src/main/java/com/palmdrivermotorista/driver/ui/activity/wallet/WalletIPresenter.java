package com.palmdrivermotorista.driver.ui.activity.wallet;

import com.palmdrivermotorista.driver.base.MvpPresenter;

import java.util.HashMap;

public interface WalletIPresenter<V extends WalletIView> extends MvpPresenter<V> {

    void getWalletData();
    void addMoney(HashMap<String, Object> obj);
}
