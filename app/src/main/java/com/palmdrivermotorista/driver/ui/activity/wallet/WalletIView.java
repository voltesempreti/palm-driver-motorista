package com.palmdrivermotorista.driver.ui.activity.wallet;

import com.palmdrivermotorista.driver.base.MvpView;
import com.palmdrivermotorista.driver.data.network.model.WalletMoneyAddedResponse;
import com.palmdrivermotorista.driver.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {

    void onSuccess(WalletResponse response);

    void onSuccess(WalletMoneyAddedResponse response);

    void onError(Throwable e);
}
