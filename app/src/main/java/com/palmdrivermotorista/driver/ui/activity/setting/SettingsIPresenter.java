package com.palmdrivermotorista.driver.ui.activity.setting;

import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface SettingsIPresenter<V extends SettingsIView> extends MvpPresenter<V> {
    void changeLanguage(String languageID);
}
