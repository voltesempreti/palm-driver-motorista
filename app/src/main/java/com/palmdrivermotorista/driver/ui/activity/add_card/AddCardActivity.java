package com.palmdrivermotorista.driver.ui.activity.add_card;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.palmdrivermotorista.driver.common.mask.MaskEditUtil;
import com.palmdrivermotorista.driver.util.ValidateCardNumber;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.palmdrivermotorista.driver.R;
import com.palmdrivermotorista.driver.base.BaseActivity;
import com.palmdrivermotorista.driver.common.Constants;
import com.palmdrivermotorista.driver.common.SharedHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class AddCardActivity extends BaseActivity implements AddCardIView {

    @BindView(R.id.submit)
    Button submit;

    private TextView card_number;
    private TextView card_expire;
    private TextView card_cvv;
    private TextView card_name;

    private ImageView card_logo;

    private TextInputEditText et_card_number;
    private TextInputEditText et_expire;
    private TextInputEditText et_cvv;
    private TextInputEditText et_name;

    private boolean findFlag = false;

    private AddCardPresenter<AddCardActivity> presenter = new AddCardPresenter<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_card;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        presenter.attachView(this);
        // Activity title will be updated after the locale has changed in Runtime
        setTitle(getString(R.string.add_card_for_payments));


        card_number = findViewById(R.id.card_number);
        card_expire = findViewById(R.id.card_expire);
        card_cvv = findViewById(R.id.card_cvv);
        card_name = findViewById(R.id.card_name);

        card_logo = findViewById(R.id.card_logo);

        et_card_number = findViewById(R.id.et_card_number);
        et_expire = findViewById(R.id.et_expire);
        et_cvv = findViewById(R.id.et_cvv);
        et_name = findViewById(R.id.et_name);

        et_name.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_expire.addTextChangedListener(MaskEditUtil.mask(et_expire, MaskEditUtil.FORMAT_DATE_CARD));

        et_card_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int count) {
                if (charSequence.toString().trim().length() == 0) {
                    card_number.setText("**** **** **** ****");
                    findFlag = false;
                    card_logo.setVisibility(View.INVISIBLE);
                } else {
                    String number = insertPeriodically(charSequence.toString().trim(), " ", 4);
                    card_number.setText(number);
                    if(!findFlag){
                        setFlagCard(number);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(et_card_number.getText().toString().equals("5")){
                    findFlag = false;
                    card_logo.setVisibility(View.INVISIBLE);
                }
            }
        });

        et_expire.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int count) {
                if (charSequence.toString().trim().length() == 0) {
                    card_expire.setText("MM/AA");
                } else {
                    String exp = insertPeriodically(charSequence.toString().trim(), "", 2);
                    card_expire.setText(exp);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        et_cvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int count) {
                if (charSequence.toString().trim().length() == 0) {
                    card_cvv.setText("***");
                } else {
                    card_cvv.setText(charSequence.toString().trim());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        et_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int count) {
                if (charSequence.toString().trim().length() == 0) {
                    card_name.setText(R.string.nome_escrito_no_cart_o);
                } else {
                    card_name.setText(charSequence.toString().trim());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    @OnClick(R.id.submit)
    public void onViewClicked() {

        if(validate()){
            int cardMonth = Integer.parseInt(et_expire.getText().toString().substring(0,2));
            int cardYear = Integer.parseInt(et_expire.getText().toString().substring(3,5));
            Card card = new Card(et_card_number.getText().toString(),
                    cardMonth,
                    cardYear,
                    et_cvv.getText().toString());
            addCard(card);
        }

    }

    public boolean validate(){

        if(et_card_number.getText().toString().equals("") ||
                et_expire.getText().toString().equals("") ||
                et_cvv.getText().toString().equals("") ||
                et_name.getText().toString().equals("")){

            Toast.makeText(this, "Por favor, preencha todos os campos!", Toast.LENGTH_SHORT).show();
            return false;
        } else if(et_card_number.getText().toString().length() != 16){
            Toast.makeText(this, "O cartão não possui 16 caracteres!", Toast.LENGTH_SHORT).show();
            return false;
        } else if(!ValidateCardNumber.isValid(et_card_number.getText().toString())){
            Toast.makeText(this, "Cartão inválido!", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public void setFlagCard(String number){

        for(int i=0; i<number.length(); i++){
            char c = number.charAt(i);
            if(i == 0){
                if(c == '4'){
                    card_logo.setVisibility(View.VISIBLE);
                    card_logo.setImageResource(R.drawable.ic_visa_new);
                    findFlag = true;
                }
            } else if(i == 1){
                if(Integer.parseInt(et_card_number.getText().toString().substring(0,2)) > 50 && Integer.parseInt(et_card_number.getText().toString().substring(0,2)) < 56){
                    card_logo.setVisibility(View.VISIBLE);
                    card_logo.setImageResource(R.drawable.mastercard);
                    findFlag = true;
                }
            }
        }

    }

    public static String insertPeriodically(String text, String insert, int period) {
        StringBuilder builder = new StringBuilder(text.length() + insert.length() * (text.length() / period) + 1);
        int index = 0;
        String prefix = "";
        while (index < text.length()) {
            builder.append(prefix);
            prefix = insert;
            builder.append(text.substring(index, Math.min(index + period, text.length())));
            index += period;
        }
        return builder.toString();
    }

    @Override
    public void onSuccess(Object card) {
        hideLoading();
        Toast.makeText(this, getString(R.string.card_added), Toast.LENGTH_SHORT).show();
        finish();
    }


    @Override
    public void onError(Throwable e) {
        hideLoading();
        if (e != null)
            onErrorBase(e);
    }

    private void addCard(Card card) {
        showLoading();
        Stripe stripe = new Stripe(this, SharedHelper.getKey(this, Constants.SharedPref.STRIPE_PUBLISHABLE_KEY));
        stripe.createToken(card,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                        hideLoading();
                        Log.d("CARD:", " " + token.getId());
                        Log.d("CARD:", " " + token.getCard().getLast4());
                        String stripeToken = token.getId();
                        showLoading();
                        presenter.addCard(stripeToken);
                    }

                    public void onError(Exception error) {
                        hideLoading();
                        Toasty.error(getApplicationContext(), error.getLocalizedMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
