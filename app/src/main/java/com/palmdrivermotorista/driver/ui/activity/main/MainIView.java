package com.palmdrivermotorista.driver.ui.activity.main;

import com.palmdrivermotorista.driver.base.MvpView;
import com.palmdrivermotorista.driver.data.network.model.EstimateFare;
import com.palmdrivermotorista.driver.data.network.model.SettingsResponse;
import com.palmdrivermotorista.driver.data.network.model.TripResponse;
import com.palmdrivermotorista.driver.data.network.model.UserResponse;

public interface MainIView extends MvpView {
    void onSuccess(UserResponse user);

    void onError(Throwable e);

    void onSuccessLogout(Object object);

    void onSuccess(TripResponse tripResponse);

    void onSuccess(SettingsResponse response);

    void onSettingError(Throwable e);

    void onSuccessProviderAvailable(Object object);

    void onSuccessFCM(Object object);

    void onSuccessLocationUpdate(TripResponse tripResponse);

    void onSuccessInstantNow(Object object);

    void onSuccessInstant(EstimateFare object);

    void onSuccess(EstimateFare estimateFare);

}
