package com.palmdrivermotorista.driver.ui.activity.your_trips;

import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface YourTripIPresenter<V extends YourTripIView> extends MvpPresenter<V> {
}
