package com.palmdrivermotorista.driver.ui.bottomsheetdialog.invoice_flow;

import com.palmdrivermotorista.driver.base.MvpView;

public interface InvoiceDialogIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
