package com.palmdrivermotorista.driver.ui.activity.welcome;

import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface WelcomeIPresenter<V extends WelcomeIView> extends MvpPresenter<V> {
}
