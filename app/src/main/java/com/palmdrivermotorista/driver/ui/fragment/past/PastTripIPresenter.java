package com.palmdrivermotorista.driver.ui.fragment.past;


import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface PastTripIPresenter<V extends PastTripIView> extends MvpPresenter<V> {

    void getHistory();

}
