package com.palmdrivermotorista.driver.ui.fragment.offline;

import com.palmdrivermotorista.driver.base.MvpView;

public interface OfflineIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
