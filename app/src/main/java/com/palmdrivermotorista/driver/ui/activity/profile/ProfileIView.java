package com.palmdrivermotorista.driver.ui.activity.profile;

import com.palmdrivermotorista.driver.base.MvpView;
import com.palmdrivermotorista.driver.data.network.model.UserResponse;

public interface ProfileIView extends MvpView {

    void onSuccess(UserResponse user);

    void onError(Throwable e);

}
