package com.palmdrivermotorista.driver.ui.activity.notification_manager;

import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface NotificationManagerIPresenter<V extends NotificationManagerIView> extends MvpPresenter<V> {
    void getNotificationManager();
}
