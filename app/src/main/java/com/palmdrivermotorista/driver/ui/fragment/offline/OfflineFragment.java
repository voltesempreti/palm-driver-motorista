package com.palmdrivermotorista.driver.ui.fragment.offline;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import androidx.drawerlayout.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gauravbhola.ripplepulsebackground.RipplePulseLayout;
import com.google.gson.Gson;
import com.palmdrivermotorista.driver.R;
import com.palmdrivermotorista.driver.base.BaseFragment;
import com.palmdrivermotorista.driver.common.Constants;
import com.palmdrivermotorista.driver.common.SharedHelper;
import com.palmdrivermotorista.driver.ui.activity.main.MainActivity;
import com.palmdrivermotorista.driver.ui.activity.wallet.WalletActivity;
import androidx.fragment.app.Fragment;
import org.json.JSONObject;
import java.util.HashMap;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import ir.sohreco.circularpulsingbutton.CircularPulsingButton;

import static com.palmdrivermotorista.driver.common.fcm.MyFireBaseMessagingService.INTENT_FILTER;

public class OfflineFragment extends BaseFragment implements OfflineIView {

    private OfflinePresenter presenter = new OfflinePresenter();

    @BindView(R.id.menu_img)
    ImageView menuImg;
    @BindView(R.id.tvApprovalDesc)
    TextView tvApprovalDesc;
    @BindView(R.id.swipeBtnEnabled)
    CircularPulsingButton swipeBtnEnabled;
    @BindView(R.id.btnPay)
    Button btnPay;
    @BindView(R.id.layout_ripplepulse)
    RipplePulseLayout layoutRipplepulse;

    private DrawerLayout drawer;
    private Context context;

    @Override
    public Fragment fragmentInstance() {
        return OfflineFragment.this;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_offline;
    }

    @Override
    public View initView(View view) {
        ButterKnife.bind(this, view);

        this.context = getContext();
        presenter.attachView(this);

        drawer = activity().findViewById(R.id.drawer_layout);
        layoutRipplepulse.startRippleAnimation();

        String s = getArguments().getString("status");
        if (!TextUtils.isEmpty(s))
            if (s.equalsIgnoreCase(Constants.User.Account.ONBOARDING))
                tvApprovalDesc.setVisibility(View.VISIBLE);
            else if (s.equalsIgnoreCase(Constants.User.Account.BANNED)) {
                tvApprovalDesc.setVisibility(View.VISIBLE);
                tvApprovalDesc.setText(getString(R.string.banned_desc));
            } else if (s.equalsIgnoreCase(Constants.User.Account.BALANCE)) {
                swipeBtnEnabled.setVisibility(View.INVISIBLE);
                tvApprovalDesc.setVisibility(View.VISIBLE);
                tvApprovalDesc.setText(getString(R.string.low_balance) + "\n\nTotal:" + Constants.Currency + Constants.userWalletBalance);
                btnPay.setVisibility(View.VISIBLE);
            }

        return view;
    }

    @SuppressLint("WrongConstant")
    @OnClick({R.id.menu_img, R.id.btnPay, R.id.swipeBtnEnabled})
    public void onViewClicked(View view) {
        this.context = getContext();
        switch (view.getId()) {
            case R.id.menu_img:
                drawer.openDrawer(Gravity.START);
                break;

            case R.id.btnPay:
                startActivity(new Intent(this.context, WalletActivity.class));
                break;
            case R.id.swipeBtnEnabled:
                HashMap<String, Object> map = new HashMap<>();
                map.put("service_status", Constants.User.Service.ACTIVE);
                SharedHelper.putKey(getContext(), "statusDriver", "online");
                Log.i("TestOnline", SharedHelper.getKey(getContext(), "statusDriver"));
                presenter.providerAvailable(map);
                break;
        }
    }

    @Override
    public void onSuccess(Object object) {
        Log.i("TestOnline", "OfflineFragment: "+SharedHelper.getKey(getContext(), "statusDriver"));
        MainActivity.navInstantRide.setVisibility(View.VISIBLE);
        try {
            JSONObject jsonObj = new JSONObject(new Gson().toJson(object));
            if (jsonObj.has("error"))
                Toasty.error(activity(), jsonObj.optString("error"), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        context.sendBroadcast(new Intent(INTENT_FILTER));
    }

    @Override
    public void onError(Throwable e) {
        hideLoading();
        //swipeBtnEnabled.toggleState();
        if (e != null) try {
            onErrorBase(e);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
