package com.palmdrivermotorista.driver.ui.activity.summary;


import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface SummaryIPresenter<V extends SummaryIView> extends MvpPresenter<V> {

    void getSummary(String data);
}
