package com.palmdrivermotorista.driver.ui.activity.change_password;

import com.palmdrivermotorista.driver.base.MvpView;

public interface ChangePasswordIView extends MvpView {


    void onSuccess(Object object);
    void onError(Throwable e);
}
