package com.palmdrivermotorista.driver.ui.activity.reset_password;

import com.palmdrivermotorista.driver.base.MvpView;

public interface ResetIView extends MvpView{

    void onSuccess(Object object);
    void onError(Throwable e);
}
