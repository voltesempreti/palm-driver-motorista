package com.palmdrivermotorista.driver.ui.activity.email;

import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface EmailIPresenter<V extends EmailIView> extends MvpPresenter<V> {
}
