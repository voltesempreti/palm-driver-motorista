package com.palmdrivermotorista.driver.ui.activity.profile;

import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface ProfileIPresenter<V extends ProfileIView> extends MvpPresenter<V> {

    void getProfile();

}
