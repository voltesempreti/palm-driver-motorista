package com.palmdrivermotorista.driver.ui.activity.invite_friend;

import com.palmdrivermotorista.driver.base.MvpView;
import com.palmdrivermotorista.driver.data.network.model.UserResponse;

public interface InviteFriendIView extends MvpView {

    void onSuccess(UserResponse response);
    void onError(Throwable e);

}
