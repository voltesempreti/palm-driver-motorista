package com.palmdrivermotorista.driver.ui.activity.earnings;


import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface EarningsIPresenter<V extends EarningsIView> extends MvpPresenter<V> {

    void getEarnings();
}
