package com.palmdrivermotorista.driver.ui.activity.forgot_password;

import com.palmdrivermotorista.driver.base.MvpView;
import com.palmdrivermotorista.driver.data.network.model.ForgotResponse;

public interface ForgotIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);
    void onError(Throwable e);
}
