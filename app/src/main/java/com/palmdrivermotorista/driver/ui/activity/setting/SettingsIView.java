package com.palmdrivermotorista.driver.ui.activity.setting;

import com.palmdrivermotorista.driver.base.MvpView;

public interface SettingsIView extends MvpView {

    void onSuccess(Object o);

    void onError(Throwable e);

}
