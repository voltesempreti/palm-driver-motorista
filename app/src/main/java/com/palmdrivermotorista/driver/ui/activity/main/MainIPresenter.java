package com.palmdrivermotorista.driver.ui.activity.main;

import com.palmdrivermotorista.driver.base.MvpPresenter;

import java.util.HashMap;
import java.util.Map;

public interface MainIPresenter<V extends MainIView> extends MvpPresenter<V> {

    void getProfile();

    void logout(HashMap<String, Object> obj);

    void getTrip(HashMap<String, Object> params);

    void providerAvailable(HashMap<String, Object> obj);

    void instantRideSendRequest(HashMap<String, Object> obj);

    void instantRideEstimateFare(HashMap<String, Object> obj);

    void requestInstantRide(Map<String, Object> params);

//    void sendFCM(JsonObject jsonObject);

    void getTripLocationUpdate(HashMap<String, Object> params);

    void getSettings();


}
