package com.palmdrivermotorista.driver.ui.bottomsheetdialog.rating;

import com.palmdrivermotorista.driver.base.MvpView;
import com.palmdrivermotorista.driver.data.network.model.Rating;

public interface RatingDialogIView extends MvpView {

    void onSuccess(Rating rating);
    void onError(Throwable e);
}
