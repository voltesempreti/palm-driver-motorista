package com.palmdrivermotorista.driver.ui.activity.invite;

import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface InviteIPresenter<V extends InviteIView> extends MvpPresenter<V> {
}
