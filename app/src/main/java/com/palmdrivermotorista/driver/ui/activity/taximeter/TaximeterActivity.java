package com.palmdrivermotorista.driver.ui.activity.taximeter;

import androidx.appcompat.widget.Toolbar;

import android.os.SystemClock;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import com.palmdrivermotorista.driver.R;
import com.palmdrivermotorista.driver.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaximeterActivity extends BaseActivity {

    private long timeWhenStopped = 0;
    private boolean startClicked = false;
    private Chronometer chronometer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.start_button)
    Button start;
    @BindView(R.id.stop_button)
    Button stop;

    @Override
    public int getLayoutId() {
        return R.layout.activity_taximeter;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        //presenter.attachView(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.tax_metro));

        chronometer = (Chronometer) findViewById(R.id.chronometer);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!startClicked){
                    chronometer.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
                    chronometer.start();
                    start.setText("PAUSAR");
                    startClicked = true;
                    stop.setVisibility(View.VISIBLE);
                } else{
                    TextView secondsText = (TextView) findViewById(R.id.hmsTekst);
                    timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                    int seconds = (int) timeWhenStopped / 1000;
                    secondsText.setText( Math.abs(seconds) + " seconds");
                    chronometer.stop();
                    start.setText("RETOMAR");
                    startClicked = false;
                }

            }
        });

        findViewById(R.id.stop_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    public void resetButtonClick(View v) {
        chronometer.setBase(SystemClock.elapsedRealtime());
        timeWhenStopped = 0;
        TextView secondsText = (TextView) findViewById(R.id.hmsTekst);
        secondsText.setText("0 seconds");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}


