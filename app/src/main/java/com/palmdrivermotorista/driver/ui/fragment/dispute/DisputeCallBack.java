package com.palmdrivermotorista.driver.ui.fragment.dispute;

public interface DisputeCallBack {
    void onDisputeCreated();
}
