package com.palmdrivermotorista.driver.ui.activity.invite_friend;

import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
