package com.palmdrivermotorista.driver.ui.activity.main;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;

import com.chaos.view.PinView;
import com.gauravbhola.ripplepulsebackground.RipplePulseLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.palmdrivermotorista.driver.BuildConfig;
import com.palmdrivermotorista.driver.MvpApplication;
import com.palmdrivermotorista.driver.R;
import com.palmdrivermotorista.driver.base.BaseActivity;
import com.palmdrivermotorista.driver.common.ChatHeadService;
import com.palmdrivermotorista.driver.common.CommonValidation;
import com.palmdrivermotorista.driver.common.Constants;
import com.palmdrivermotorista.driver.common.FloatWidgetService;
import com.palmdrivermotorista.driver.common.GPSTracker;
import com.palmdrivermotorista.driver.common.LocaleHelper;
import com.palmdrivermotorista.driver.common.PolyUtil;
import com.palmdrivermotorista.driver.common.SharedHelper;
import com.palmdrivermotorista.driver.common.Utilities;
import com.palmdrivermotorista.driver.common.chat.ChatActivity;
import com.palmdrivermotorista.driver.common.fcm.MyFireBaseMessagingService;
import com.palmdrivermotorista.driver.common.mask.MaskEditUtil;
import com.palmdrivermotorista.driver.data.network.model.EstimateFare;
import com.palmdrivermotorista.driver.data.network.model.LatLngFireBaseDB;
import com.palmdrivermotorista.driver.data.network.model.SettingsResponse;
import com.palmdrivermotorista.driver.data.network.model.TripResponse;
import com.palmdrivermotorista.driver.data.network.model.UserResponse;
import com.palmdrivermotorista.driver.ui.activity.add_card.AddCardActivity;
import com.palmdrivermotorista.driver.ui.activity.card.CardActivity;
import com.palmdrivermotorista.driver.ui.activity.document.DocumentActivity;
import com.palmdrivermotorista.driver.ui.activity.earnings.EarningsActivity;
import com.palmdrivermotorista.driver.ui.activity.help.HelpActivity;
import com.palmdrivermotorista.driver.ui.activity.instant_ride.InstantRideActivity;
import com.palmdrivermotorista.driver.ui.activity.invite.InviteActivity;
import com.palmdrivermotorista.driver.ui.activity.invite_friend.InviteFriendActivity;
import com.palmdrivermotorista.driver.ui.activity.location_pick.LocationPickActivity;
import com.palmdrivermotorista.driver.ui.activity.notification_manager.NotificationManagerActivity;
import com.palmdrivermotorista.driver.ui.activity.profile.ProfileActivity;
import com.palmdrivermotorista.driver.ui.activity.profile_update.ProfileUpdateActivity;
import com.palmdrivermotorista.driver.ui.activity.summary.SummaryActivity;
import com.palmdrivermotorista.driver.ui.activity.taximeter.TaximeterActivity;
import com.palmdrivermotorista.driver.ui.activity.wallet.WalletActivity;
import com.palmdrivermotorista.driver.ui.activity.your_trips.YourTripActivity;
import com.palmdrivermotorista.driver.ui.bottomsheetdialog.invoice_flow.InvoiceDialogFragment;
import com.palmdrivermotorista.driver.ui.bottomsheetdialog.rating.RatingDialogFragment;
import com.palmdrivermotorista.driver.ui.fragment.incoming_request.IncomingRequestFragment;
import com.palmdrivermotorista.driver.ui.fragment.offline.OfflineFragment;
import com.palmdrivermotorista.driver.ui.fragment.status_flow.StatusFlowFragment;
import com.palmdrivermotorista.driver.ui.fragment.navigation.NavigationFragment;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import ir.sohreco.circularpulsingbutton.CircularPulsingButton;

import static com.palmdrivermotorista.driver.MvpApplication.DATUM;
import static com.palmdrivermotorista.driver.MvpApplication.DEFAULT_ZOOM;
import static com.palmdrivermotorista.driver.MvpApplication.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION;
import static com.palmdrivermotorista.driver.MvpApplication.PICK_LOCATION_REQUEST_CODE;
import static com.palmdrivermotorista.driver.MvpApplication.canGoToChatScreen;
import static com.palmdrivermotorista.driver.MvpApplication.isChatScreenOpen;
import static com.palmdrivermotorista.driver.MvpApplication.mLastKnownLocation;
import static com.palmdrivermotorista.driver.common.Constants.checkStatus.ARRIVED;
import static com.palmdrivermotorista.driver.common.Constants.checkStatus.PICKEDUP;
import static com.palmdrivermotorista.driver.common.Constants.checkStatus.STARTED;

public class MainActivity extends BaseActivity implements
        MainIView,
        NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraIdleListener,
        DirectionCallback {

    private static final int APP_PERMISSION_REQUEST = 102;
    private static final int CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE = 104;
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.top_layout)
    LinearLayout topLayout;
    @BindView(R.id.lnrLocationHeader)
    LinearLayout lnrLocationHeader;
    @BindView(R.id.lblLocationType)
    TextView lblLocationType;
    @BindView(R.id.lblLocationName)
    TextView lblLocationName;
    @BindView(R.id.offline_container)
    FrameLayout offlineContainer;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.gps)
    ImageView gps;
    @BindView(R.id.navigation_img)
    ImageView navigationImg;
    @BindView(R.id.sbChangeStatus)
    CircularPulsingButton sbChangeStatus;
    @BindView(R.id.style_map)
    ImageView styleMapButton;
    @BindView(R.id.layout_ripplepulse)
    RipplePulseLayout layoutRipplepulse;

    TextView lblMenuName, lblMenuEmail;
    public static MainPresenter presenter = new MainPresenter();
    SupportMapFragment mapFragment;
    GoogleMap googleMap;

    LinearLayout lnrVerification;
    LinearLayout lnrTimeTravel;
    public static LinearLayout navInstantRide;
    LinearLayout llOTP;
    TextView instantDestination, estimateFare, timeTravel;
    PinView pinView;
    AlertDialog otpDialog;
    String server_otp = "", user_id_now = "";
    private String numberPhone = "";
    NumberFormat numberFormat;
    private AlertDialog alertDialog;

    private NavigationView navigationView;
    private ImageView picture;
    private TextView name;

    private Runnable r;
    private Handler h;
    private int delay = 5000;
    private int countRequest = 0;
    private String STATUS = "";
    private String CURRENT_DEST_ADDRESS = "";
    private String ACCOUNTSTATUS = "";
    private boolean mLocationPermissionGranted;
    private FusedLocationProviderClient mFusedLocation;
    private BottomSheetBehavior bottomSheetBehavior;
    private Intent gpsServiceIntent;
    private Intent floatingWidgetIntent;
    private DatabaseReference mProviderLocation;
    private ArrayList<LatLng> polyLinePoints;
    private Polyline mPolyline;
    private boolean canReRoute = true, canCarAnim = true;
    private LatLng start = null, end = null;
    //SETAR ID DOS TIPOS DE SERVIÇO
    private static Integer serviceMototaxiID = 1;
    private static Integer serviceMotoboyID = 4;
    private static MediaPlayer mediaPlayerBeep;

    private int mapDark = R.raw.style_map_dark;
    private int mapDefault = R.raw.style_map_default;
    private int styleMap = 0;

    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("TTTTT", "BroadcastReceiver");
            HashMap<String, Object> params = new HashMap<>();
            if (mLastKnownLocation != null) {
                params.put("latitude", mLastKnownLocation.getLatitude());
                params.put("longitude", mLastKnownLocation.getLongitude());
            }
            Log.i("LOG", "Atualizando Trip " + new Timer().toString());
            presenter.getTrip(params);
        }
    };
    private int canMapAnimate;
    private Marker srcMarker;

    public static void startFloatingViewService(Activity activity) {
        Log.i("TTTTT", "startFloatingViewService");
        // *** You must follow these rules when obtain the cutout(FloatingViewManager.findCutoutSafeArea) ***
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // 1. 'windowLayoutInDisplayCutoutMode' do not be set to 'never'
            if (activity.getWindow().getAttributes().layoutInDisplayCutoutMode == WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_NEVER) {
                throw new RuntimeException("'windowLayoutInDisplayCutoutMode' do not be set to 'never'");
            }
            // 2. Do not set Activity to landscape
            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                throw new RuntimeException("Do not set Activity to landscape");
            }
        }

        final Class<? extends Service> service;
        service = ChatHeadService.class;

        final Intent intent = new Intent(activity, service);
        //intent.putExtra(ChatHeadService.EXTRA_CUTOUT_SAFE_AREA, FloatingViewManager.findCutoutSafeArea(activity));
        ContextCompat.startForegroundService(activity, intent);
    }

    @Override
    public int getLayoutId() {
        Log.i("TTTTT", "getLayoutId");
        return R.layout.activity_main;
    }

    public void showNotification(){
        Log.i("TTTTT", "showNotification");
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId =  "prologedChannelID_1";//getString(R.string.default_notification_channel_id);
        Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.beep);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Creating Channel
            @SuppressLint("WrongConstant")
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title", NotificationManager.IMPORTANCE_HIGH);

            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT)
                    .build();

            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.setDescription("AKsbadkbJSANDASJKDNVLAKDJSNV");
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            channel.enableVibration(true);
            channel.setSound(alarmSound, attributes);
            nm.createNotificationChannel(channel);

        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_car_notification)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("ALSDJNVAFDJNV;ALDFNL;SV")
                .setTicker("Hearty365")
                .setContentIntent(pIntent)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSound(alarmSound);

        Notification mNotification = notificationBuilder.build();

        nm.notify(0, mNotification);
    }

    @Override
    public void initView() {
        Log.i("TTTTT", "initview");
        ButterKnife.bind(this);
        presenter.attachView(this);
        mediaPlayerBeep = MediaPlayer.create(this, R.raw.beep);
        registerReceiver(myReceiver, new IntentFilter(MyFireBaseMessagingService.INTENT_FILTER));

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        navInstantRide = findViewById(R.id.nav_instant_ride);
        layoutRipplepulse.startRippleAnimation();

        //Configura o estilo do mapa
        if(SharedHelper.getKey(this, "style_map").isEmpty()){
            styleMap = 0;
            styleMapButton.setImageResource(R.drawable.ic_auto);
        } else{
            styleMap = Integer.parseInt(SharedHelper.getKey(this, "style_map"));
            if(styleMap == R.raw.style_map_dark){
                styleMapButton.setImageResource(R.drawable.ic_moon);
            } else if(styleMap == R.raw.style_map_default){
                styleMapButton.setImageResource(R.drawable.ic_sun);
            } else{
                styleMapButton.setImageResource(R.drawable.ic_auto);
            }
        }

        checkStatusDriver();
        checkGps();
        numberFormat = getNumberFormat();

        // Verifica permissão nos dispositivos xiomi, hauwei etc
//        Utilities.startPowerSaverIntent(this);
//
//        gps.setOnClickListener(v -> {
//
//            final Handler handler = new Handler();
//            handler.postDelayed(() -> runOnUiThread(this::showNotification), 5000);
//
//
//        });

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        picture = findViewById(R.id.nav_image);
        name = findViewById(R.id.nav_name);
        //sub_name = findViewById(R.id.nav_email);

        setActionsMenu();

        mFusedLocation = LocationServices.getFusedLocationProviderClient(this);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        bottomSheetBehavior = BottomSheetBehavior.from(container);
        bottomSheetBehavior.setHideable(false);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // Un used
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // Un used
            }
        });

        String profileUrl = getIntent().getStringExtra("avartar");
        if (profileUrl != null && !profileUrl.isEmpty())
            Glide.with(activity())
                    .load(profileUrl)
                    .apply(RequestOptions
                            .placeholderOf(R.drawable.ic_user_placeholder)
                            .dontAnimate()
                            .error(R.drawable.ic_user_placeholder))
                    .into(picture);

        if (floatingWidgetIntent == null)
            floatingWidgetIntent = new Intent(MainActivity.this, FloatWidgetService.class);

        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View view, float v) {

            }

            @Override
            public void onDrawerOpened(@NonNull View view) {
                presenter.getSettings();
            }

            @Override
            public void onDrawerClosed(@NonNull View view) {

            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });

        showFloatingView(activity(), true);

    }

    public void checkStatusDriver(){

        if(SharedHelper.getKey(this, "statusDriver") != null){
            if(SharedHelper.getKey(this, "statusDriver").equals("offline")){
                Log.i("TestOnline", "Gone");
                navInstantRide.setVisibility(View.GONE);
            } else{
                Log.i("TestOnline", "Visible: "+ SharedHelper.getKey(this, "statusDriver"));
                navInstantRide.setVisibility(View.VISIBLE);
            }
        }

    }

    public void checkGps(){

        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);

// Verifica se o GPS está ativo
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

// Caso não esteja ativo abre um novo diálogo com as configurações para
// realizar se ativamento
        if (!enabled) {
            alertDialog = new AlertDialog.Builder(this)
                    .setTitle("Atenção")
                    .setMessage("O GPS do aparelho encontra-se desativado, prossiga para ativa-lo!")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                            alertDialog.dismiss();
                        }
                    })
                    .setCancelable(false)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

    }

    @SuppressLint("NewApi")
    public void showFloatingView(Context context, boolean isShowOverlayPermission) {
        Log.i("TTTTT", "showFloatingView");
        // API22以下かチェック
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startFloatingViewService(activity());
            return;
        }

        if (Settings.canDrawOverlays(context)) {
            startFloatingViewService(activity());
            return;
        }

        if (isShowOverlayPermission) {
            final Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + context.getPackageName()));
            startActivityForResult(intent, CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE);
        }
    }

    private void startCheckStatusCall() {
        Log.i("TTTTT", "startCheckStatusCall");
        try {
            h = new Handler();
            r = () -> {
                if (mLastKnownLocation != null) {
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("latitude", mLastKnownLocation.getLatitude());
                    params.put("longitude", mLastKnownLocation.getLongitude());
                    presenter.getTrip(params);

//                    //Seta posição atual do usuário nas constantes
//                    Constants.CURRENT_LAT = googleMap.getMyLocation().getLatitude();
//                    Constants.CURRENT_LNG = googleMap.getMyLocation().getLongitude();

                } else if (DATUM != null) {
                    if (DATUM.getStatus().equals(STARTED) || DATUM.getStatus().equals(ARRIVED)
                            || DATUM.getStatus().equals(PICKEDUP)) if (canMapAnimate % 3 == 0) {
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        for (LatLng latLng : polyLinePoints) builder.include(latLng);
                        LatLngBounds bounds = builder.build();
                        try {
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 250));
                        } catch (Exception e) {
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 90));
                        }
                    }
                }
                canMapAnimate++;
                h.postDelayed(r, delay);
            };
            h.postDelayed(r, delay);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("TTTTT", "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_LOCATION_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (RIDE_REQUEST.containsKey("d_address")) {
                    instantDestination.setText(String.valueOf(RIDE_REQUEST.get("d_address")));
                } else {
                    instantDestination.setText("");
                }
            }
        }

        if (requestCode == APP_PERMISSION_REQUEST && resultCode == RESULT_OK) openMap();
        else{
//            Toast.makeText(this, "Permissão de aplicativo não ativada.", Toast.LENGTH_SHORT).show();
        }

        if (requestCode == CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE) {
            showFloatingView(activity(), false);
        }
    }

    private void openMap() {
        Log.i("TTTTT", "openmap");
        try {
            startService(floatingWidgetIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (getString(R.string.pick_up_location).equalsIgnoreCase(lblLocationType.getText().toString())) {
            if (DATUM.getSLatitude() != 0 && DATUM.getSLongitude() != 0) {
                Bundle bn = new Bundle();
                bn.putString("lat", Double.toString(DATUM.getSLatitude()));
                bn.putString("long", Double.toString(DATUM.getSLongitude()));
                NavigationFragment navigationFragment = new NavigationFragment();
                navigationFragment.setArguments(bn);
                FragmentManager fm = getSupportFragmentManager();
                navigationFragment.show(fm, "fragment_navigation");
            }
        } else if (DATUM.getDLatitude() != 0 && DATUM.getDLongitude() != 0) {
            Bundle bn = new Bundle();
            bn.putString("lat", Double.toString(DATUM.getDLatitude()));
            bn.putString("long", Double.toString(DATUM.getDLongitude()));
            NavigationFragment navigationFragment = new NavigationFragment();
            navigationFragment.setArguments(bn);
            FragmentManager fm = getSupportFragmentManager();
            navigationFragment.show(fm, "fragment_navigation");
        }
    }

    @Override
    protected void onResume() {
        Log.i("TTTTT", "onresume");
        super.onResume();
        checkGps();
        ACCOUNTSTATUS = "";
        STATUS = "";
        gpsServiceIntent = new Intent(this, GPSTracker.class);
        startService(gpsServiceIntent);
        presenter.getProfile();
        HashMap<String, Object> params = new HashMap<>();
        if (mLastKnownLocation != null) {
            params.put("latitude", mLastKnownLocation.getLatitude());
            params.put("longitude", mLastKnownLocation.getLongitude());
            SharedHelper.putKey(this, Constants.SharedPref.LATITUDE, String.valueOf(mLastKnownLocation.getLatitude()));
            SharedHelper.putKey(this, Constants.SharedPref.LONGITUDE, String.valueOf(mLastKnownLocation.getLongitude()));
        }
        presenter.getTrip(params);
        registerReceiver(myReceiver, new IntentFilter(MyFireBaseMessagingService.INTENT_FILTER));
        startCheckStatusCall();

        NotificationManager notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    protected void onPause() {
        Log.i("TTTTT", "onpause");
        super.onPause();
        unregisterReceiver(myReceiver);
        h.removeCallbacks(r);
    }

    @Override
    protected void onDestroy() {
        Log.i("TTTTT", "ondestroy");
        super.onDestroy();

        if (floatingWidgetIntent != null)
            stopService(floatingWidgetIntent);
        if (gpsServiceIntent != null) stopService(gpsServiceIntent);

    }

    private boolean isServiceRunning() {
        Log.i("TTTTT", "isServiceRunning");
        ActivityManager manager = (ActivityManager) activity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (GPSTracker.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void setActionsMenu(){

        findViewById(R.id.nav_your_trips).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, YourTripActivity.class));
            }
        });
        findViewById(R.id.nav_wallet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, WalletActivity.class));
            }
        });
        findViewById(R.id.nav_summary).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SummaryActivity.class));
            }
        });
        findViewById(R.id.nav_earnings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, EarningsActivity.class));
            }
        });
        findViewById(R.id.nav_instant_ride).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInstantOTP();
            }
        });
        findViewById(R.id.nav_taximeter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TaximeterActivity.class));
            }
        });
        findViewById(R.id.nav_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CardActivity.class));
            }
        });
        findViewById(R.id.nav_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DocumentActivity.class);
                intent.putExtra("setting", "isClick");
                startActivity(intent);
            }
        });
        findViewById(R.id.nav_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NotificationManagerActivity.class));
            }
        });
        findViewById(R.id.nav_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToShareScreen();
            }
        });
        findViewById(R.id.nav_document).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, DocumentActivity.class));
            }
        });
        findViewById(R.id.nav_invite_friends).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, InviteFriendActivity.class));
            }
        });
        findViewById(R.id.nav_invite_referral).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, InviteActivity.class));
            }
        });
        findViewById(R.id.nav_help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, HelpActivity.class));
            }
        });
        findViewById(R.id.nav_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            }
        });
        findViewById(R.id.nav_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowLogoutPopUp();
            }
        });
        findViewById(R.id.nav_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowLogoutPopUp();
            }
        });
    }

    private void showInstantOTP() {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity());
        LayoutInflater inflater = activity().getLayoutInflater();
        View view = inflater.inflate(R.layout.instant_otp_dialog, null);

        lnrVerification = view.findViewById(R.id.lnrVerification);
        lnrTimeTravel = view.findViewById(R.id.lnrTimeOfTravel);
        llOTP = view.findViewById(R.id.llOTP);
        Button btnSubmit = view.findViewById(R.id.submit_btn);
        Button btnCancel = view.findViewById(R.id.btnCancel);

        instantDestination = view.findViewById(R.id.instant_destination);
        estimateFare = view.findViewById(R.id.estimate_fare);
        timeTravel = view.findViewById(R.id.timeTravel);

        EditText txtPhoneNumber = view.findViewById(R.id.txtPhoneNumber);
        EditText txtName = view.findViewById(R.id.txtName);
        pinView = view.findViewById(R.id.pinView);

        txtPhoneNumber.addTextChangedListener(MaskEditUtil.mask(txtPhoneNumber, MaskEditUtil.FORMAT_FONE));

        builder.setView(view);
        otpDialog = builder.create();
        otpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        instantDestination.setOnClickListener(view13 -> {
            Intent destinationIntent = new Intent(MainActivity.this, LocationPickActivity.class);
            HashMap<String, Object> mapLoc = new HashMap<>();
            mapLoc.put("location", RIDE_REQUEST.get("s_address"));
            mapLoc.put("desFocus", "true");
            destinationIntent.putExtra("hashMap", mapLoc);
            Log.i("TestLocation", "Main - "+ mapLoc.get("location"));
            startActivityForResult(destinationIntent, PICK_LOCATION_REQUEST_CODE);
        });

        btnSubmit.setOnClickListener(view12 -> {

            if (CommonValidation.Validation(txtName.getText().toString().trim())) {
                Toasty.error(activity(), "Informe o nome", Toast.LENGTH_SHORT, true).show();
            } else if (CommonValidation.Validation(txtPhoneNumber.getText().toString().trim())) {
                Toasty.error(activity(), getString(R.string.invalid_mobile), Toast.LENGTH_SHORT, true).show();
            } else if (RIDE_REQUEST.get("d_address") == null) {
                Toasty.error(activity(), "Selecione um endereço de entrega", Toast.LENGTH_SHORT, true).show();
            } else {
                HashMap<String, Object> map = new HashMap<>();
                map.put("mobile", txtPhoneNumber.getText().toString().trim());
                map.put("name", txtName.getText().toString().trim());

                numberPhone = txtPhoneNumber.getText().toString().trim();

                LatLng currentLatLng = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                String address = getAddress(currentLatLng);

                map.put("s_latitude", mLastKnownLocation.getLatitude());
                map.put("s_longitude", mLastKnownLocation.getLongitude());
                map.put("s_address", address);
                map.put("d_latitude", RIDE_REQUEST.get("d_latitude"));
                map.put("d_longitude", RIDE_REQUEST.get("d_longitude"));
                map.put("d_address", RIDE_REQUEST.get("d_address"));
                map.put("country_code", "+55");
                map.put("service_type", SharedHelper.getIntKey(this, Constants.SharedPref.SERVICE_TYPE));
                Log.i("TestEstimated", "Entrou, Tipo: "+SharedHelper.getIntKey(this, Constants.SharedPref.SERVICE_TYPE));
                presenter.instantRideEstimateFare(map);
            }
            //}

        });

        btnCancel.setOnClickListener(view1 -> otpDialog.dismiss());
        otpDialog.show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Log.i("TTTTT", "onNavigationItemSelected");
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_card:
                startActivity(new Intent(this, CardActivity.class));
                break;
            case R.id.nav_instant_ride:
                startActivity(new Intent(this, InstantRideActivity.class));
                break;
            case R.id.nav_your_trips:
                startActivity(new Intent(this, YourTripActivity.class));
                break;
            case R.id.nav_earnings:
                startActivity(new Intent(this, EarningsActivity.class));
                break;
            case R.id.nav_wallet:
                startActivity(new Intent(this, WalletActivity.class));
                break;
            case R.id.nav_summary:
                startActivity(new Intent(this, SummaryActivity.class));
                break;
            case R.id.nav_settings:
//                Intent intent = new Intent(this, SettingsActivity.class);
                Intent intent = new Intent(this, DocumentActivity.class);
                intent.putExtra("setting", "isClick");
                startActivity(intent);
                break;
            case R.id.nav_notification:
                startActivity(new Intent(this, NotificationManagerActivity.class));
                break;
            case R.id.nav_help:
                startActivity(new Intent(this, HelpActivity.class));
                break;
            case R.id.nav_share:
                navigateToShareScreen();
                break;
            case R.id.nav_document:
                startActivity(new Intent(this, DocumentActivity.class));
                break;
            case R.id.nav_invite_referral:
                startActivity(new Intent(this, InviteActivity.class));
                break;
            case R.id.nav_invite_friends:
                startActivity(new Intent(this, InviteFriendActivity.class));
                break;
            case R.id.nav_logout:
                ShowLogoutPopUp();
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public void onCameraIdle() {
        Log.i("TTTTT", "onCameraIdle");
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public void onCameraMove() {
        Log.i("TTTTT", "onCameraMove");
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i("TTTTT", "onMapReady");
        try {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));
        } catch (Resources.NotFoundException e) {
            Log.d("Map:Style", "Can't find style. Error: ");
        }
        this.googleMap = googleMap;

        //boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources().getString(R.string.style_json)));

        boolean success = true;

        if(styleMap == 0){ //se tiver no automatico
            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);

            if(hour > 17 || hour < 6){
                success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_map_dark));
                Log.i("TestTypeMap", "Automatico - Dark map");
            } else{
                success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_map_default));
                Log.i("TestTypeMap", "Automatico - Default map");
            }
        } else{
            success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, styleMap));
            Log.i("TestTypeMap", "Mapa definido");
        }

        getLocationPermission();
        updateLocationUI();
        getDeviceLocation();

        boolean serviceRunningStatus = isServiceRunning();

        if (serviceRunningStatus) {
            Intent serviceIntent = new Intent(this, GPSTracker.class);
            stopService(serviceIntent);
        }
        if (!serviceRunningStatus) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                activity().startService(new Intent(activity(), GPSTracker.class));
            } else {
                Intent serviceIntent = new Intent(activity(), GPSTracker.class);
                ContextCompat.startForegroundService(activity(), serviceIntent);
            }
        }
    }

    @SuppressLint("WrongConstant")
    @OnClick({R.id.menu, R.id.nav_view, R.id.navigation_img, R.id.gps, R.id.style_map, R.id.sbChangeStatus})
    public void onViewClicked(View view) {
        Log.i("TTTTT", "onViewClicked");
        switch (view.getId()) {
            case R.id.menu:

                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                else {
                    UserResponse user = new Gson().fromJson(SharedHelper.getKey(this, "userInfo"), UserResponse.class);


                    if (user != null) {
                        SharedHelper.putKey(this, Constants.SharedPref.CURRENCY, user.getCurrency());
                        Constants.Currency = SharedHelper.getKey(this, Constants.SharedPref.CURRENCY);

                        name.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
                        //lblMenuEmail.setText(user.getEmail());
                        SharedHelper.putKey(activity(), Constants.SharedPref.PICTURE, user.getAvatar());
                        Glide.with(activity())
                                .load(BuildConfig.BASE_IMAGE_URL + user.getAvatar())
                                .apply(RequestOptions.placeholderOf(R.drawable.ic_user_placeholder)
                                        .dontAnimate()
                                        .error(R.drawable.ic_user_placeholder))
                                .into(picture);
                    } else presenter.getProfile();
                    drawerLayout.openDrawer(Gravity.START);
                }

                break;
            case R.id.nav_view:
                break;
            case R.id.gps:
                if (mLastKnownLocation != null) {
                    LatLng currentLatLng = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, DEFAULT_ZOOM));
                }
                break;
            case R.id.navigation_img:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this))
                    startActivityForResult(new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + getPackageName())), APP_PERMISSION_REQUEST);
                else openMap();
                break;
            case R.id.style_map:
                if(styleMap == 0){
                    styleMapButton.setImageResource(R.drawable.ic_moon);
                    SharedHelper.putKey(this, "style_map", ""+R.raw.style_map_dark);
                    Log.i("TestTypeMap", "Setou dark");
                    styleMap = R.raw.style_map_dark;
                    Intent intent2 = getIntent();
                    finish();
                    startActivity(intent2);
                } else if(styleMap == R.raw.style_map_dark){
                    styleMapButton.setImageResource(R.drawable.ic_sun);
                    SharedHelper.putKey(this, "style_map", ""+R.raw.style_map_default);
                    Log.i("TestTypeMap", "Setou claro");
                    styleMap = R.raw.style_map_default;
                    Intent intent2 = getIntent();
                    finish();
                    startActivity(intent2);
                } else{
                    styleMapButton.setImageResource(R.drawable.ic_auto);
                    SharedHelper.putKey(this, "style_map", ""+0);
                    Log.i("TestTypeMap", "Setou default");
                    styleMap = 0;
                    Intent intent2 = getIntent();
                    finish();
                    startActivity(intent2);
                }
                break;
            case R.id.sbChangeStatus:
                HashMap<String, Object> map = new HashMap<>();
                map.put("service_status", "offline");
                SharedHelper.putKey(this, "statusDriver", "offline");
                Log.i("TestOnline", "Status: "+SharedHelper.getKey(this, "statusDriver"));
                presenter.providerAvailable(map);
                break;
        }
    }

    public void getLocationPermission() {
        Log.i("TTTTT", "getLocationPermission");
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) mLocationPermissionGranted = true;
        else
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
    }

    private void updateLocationUI() {
        Log.i("TTTTT", "updateLocationUI");
        if (googleMap == null) return;
        try {
            if (mLocationPermissionGranted) {
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                googleMap.getUiSettings().setCompassEnabled(false);
                googleMap.setOnCameraMoveListener(this);
                googleMap.setOnCameraIdleListener(this);
            } else {
                googleMap.setMyLocationEnabled(false);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                googleMap.getUiSettings().setCompassEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    void getDeviceLocation() {
        Log.i("TTTTT", "getDeviceLocation");
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocation.getLastLocation();
                locationResult.addOnCompleteListener(this, task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = task.getResult();
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                                        mLastKnownLocation.getLatitude(),
                                        mLastKnownLocation.getLongitude()),
                                DEFAULT_ZOOM));
                    } else {
                        Log.e("Map", "Current location is null. Using defaults.: %s", task.getException());
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                });
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.i("TTTTT", "onRequestPermissionsResult");
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                    updateLocationUI();
                    getDeviceLocation();
                }
            }
        }
    }

    private void changeFragment(Fragment fragment) {

        Log.i("TTTTT", "changeFragment");
        if (isFinishing()) return;

        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, fragment, fragment.getTag());
            transaction.commitAllowingStateLoss();
            sbChangeStatus.setVisibility(View.GONE);
        } else {
            if (IncomingRequestFragment.countDownTimer != null) {
                IncomingRequestFragment.countDownTimer.cancel();
                if (IncomingRequestFragment.mPlayer.isPlaying())
                    IncomingRequestFragment.mPlayer.stop();
            }
            container.removeAllViews();
            //sbChangeStatus.collapseButton();
            sbChangeStatus.setVisibility(View.VISIBLE);
            lnrLocationHeader.setVisibility(View.GONE);
            googleMap.clear();
        }
    }

    private void offlineFragment(String s) {
        Log.i("TTTTT", "offlineFragment");
        Fragment fragment = new OfflineFragment();
        Bundle b = new Bundle();
        b.putString("status", s);
        fragment.setArguments(b);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.offline_container, fragment, fragment.getTag());
        transaction.commitAllowingStateLoss();
        ACCOUNTSTATUS = "";
    }

    @Override
    public void onSuccess(SettingsResponse response) {
        Log.i("TTTTT", "onSuccess SettingsResponse");
        if (response.getReferral().getReferral().equalsIgnoreCase("1")) navMenuVisibility(true);
        else navMenuVisibility(false);
    }

    @Override
    public void onSettingError(Throwable e) {
        Log.i("TTTTT", "onSettingError");
        navMenuVisibility(false);
    }

    private void navMenuVisibility(boolean visibility) {
        Log.i("TTTTT", "navMenuVisibility");
        Menu nav_Menu = navView.getMenu();
        MenuItem nav_invite_friend = nav_Menu.findItem(R.id.nav_invite_friends);
        //nav_invite_friend.setVisible(visibility);
    }

    @Override
    public void onSuccess(UserResponse user) {
        Log.i("TTTTT", "onSuccess UserResponse");
        if (user != null) {

            //SALVA DADOS DO USUÁRIO
            Constants.userAvatar = user.getAvatar();
            Constants.userName = user.getFirstName() + " " + user.getLastName();
            Constants.userEmail = user.getEmail();
            Constants.userCpf = user.getCpf();
            Constants.userSos = user.getSos();
            String userWalletBalance = String.valueOf(user.getWalletBalance());
            userWalletBalance = userWalletBalance.replaceAll("-", "");
            Constants.userWalletBalance = userWalletBalance;
            Constants.userCard = user.getCard();

            String dd = LocaleHelper.getLanguage(this);
            if (user.getProfile() != null && user.getProfile().getLanguage() != null &&
                    !user.getProfile().getLanguage().equalsIgnoreCase(dd)) {
                LocaleHelper.setLocale(getApplicationContext(), user.getProfile().getLanguage());
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK));
            }
            name.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
            //lblMenuEmail.setText(user.getEmail());
            SharedHelper.putKey(activity(), Constants.SharedPref.PICTURE, user.getAvatar());
            Glide.with(activity())
                    .load(BuildConfig.BASE_IMAGE_URL + user.getAvatar())
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_user_placeholder)
                            .dontAnimate()
                            .error(R.drawable.ic_user_placeholder))
                    .into(picture);
            SharedHelper.putKey(this, Constants.SharedPref.STRIPE_PUBLISHABLE_KEY, user.getStripePublishableKey());
            SharedHelper.putKey(this, Constants.SharedPref.USER_ID, String.valueOf(user.getId()));
            SharedHelper.putKey(this, Constants.SharedPref.USER_NAME, user.getFirstName()
                    + " " + user.getLastName());
            SharedHelper.putKey(this, Constants.SharedPref.USER_AVATAR, BuildConfig.BASE_IMAGE_URL + user.getAvatar());
            SharedHelper.putKey(this, Constants.SharedPref.CURRENCY, user.getCurrency());
            SharedHelper.putKey(this, Constants.SharedPref.SERVICE_TYPE, user.getService().getServiceType().getId());
            SharedHelper.putKey(this, Constants.SharedPref.USER_INFO, printJSON(user));
            Constants.Currency = SharedHelper.getKey(this, Constants.SharedPref.CURRENCY);
            int card = user.getCard();
            if (card == 0) {
                Menu nav_Menu = navView.getMenu();
//                nav_Menu.findItem(R.id.nav_card).setVisible(false);
            } else {
                Menu nav_Menu = navView.getMenu();
//                nav_Menu.findItem(R.id.nav_card).setVisible(true);
            }
            SharedHelper.putKey(this, "card", card);

            SharedHelper.putKey(this, Constants.ReferalKey.REFERRAL_CODE, user.getReferral_unique_id());
            SharedHelper.putKey(this, Constants.ReferalKey.REFERRAL_COUNT, user.getReferral_count());
            SharedHelper.putKey(this, Constants.ReferalKey.REFERRAL_TEXT, user.getReferral_text());
            SharedHelper.putKey(this, Constants.ReferalKey.REFERRAL_TOTAL_TEXT, user.getReferral_total_text());
        }
    }

    @Override
    public void onError(Throwable e) {
        Log.i("TTTTT", "onError");
        hideLoading();
        if (e != null)
            onErrorBase(e);
    }

    @Override
    public void onSuccessLogout(Object object) {
        Log.i("TTTTT", "onSuccessLogout");
        Utilities.LogoutApp(activity(), "");
    }

    @Override
    public void onSuccess(TripResponse response) {
        Log.i("TTTTT", "onSuccess");
        String accountStatus = response.getAccountStatus();
        String serviceStatus = response.getServiceStatus();

        MvpApplication.tripResponse = response;

        if (!ACCOUNTSTATUS.equalsIgnoreCase(accountStatus)) {
            ACCOUNTSTATUS = accountStatus;
            if (accountStatus.equalsIgnoreCase(Constants.User.Account.PENDING_DOCUMENT)) {
                startActivity(new Intent(MainActivity.this, DocumentActivity.class));
                //imgStatus.setImageResource(R.drawable.banner_waiting);
            } else if (accountStatus.equalsIgnoreCase(Constants.User.Account.PENDING_CARD)) {
                startActivity(new Intent(MainActivity.this, AddCardActivity.class));
                //imgStatus.setImageResource(R.drawable.banner_waiting);
            } else if (accountStatus.equalsIgnoreCase(Constants.User.Account.ONBOARDING)) {
                offlineFragment(Constants.User.Account.ONBOARDING);
                //imgStatus.setImageResource(R.drawable.banner_waiting);
            } else if (Constants.User.Account.BANNED.equalsIgnoreCase(accountStatus)) {
                offlineFragment(Constants.User.Account.BANNED);
                //imgStatus.setImageResource(R.drawable.banner_banned);
            } else if (Constants.User.Account.APPROVED.equalsIgnoreCase(accountStatus)
                    && Constants.User.Service.OFFLINE.equalsIgnoreCase(serviceStatus)) {
                offlineFragment(Constants.User.Service.OFFLINE);
                //imgStatus.setImageResource(R.drawable.banner_active);

                //Verifica cadastro incompleto
                if (Constants.userAvatar == null && mLocationPermissionGranted || Constants.userCpf == null && mLocationPermissionGranted) {
                    startActivity(new Intent(MainActivity.this, ProfileUpdateActivity.class));
                }
            } else if (Constants.User.Account.APPROVED.equalsIgnoreCase(accountStatus)
                    && Constants.User.Service.ACTIVE.equalsIgnoreCase(serviceStatus)) {
                offlineContainer.removeAllViews();
                //imgStatus.setImageResource(R.drawable.banner_active);

                //Verifica cadastro incompleto
                if (Constants.userAvatar == null && mLocationPermissionGranted || Constants.userCpf == null && mLocationPermissionGranted) {
                    startActivity(new Intent(MainActivity.this, ProfileUpdateActivity.class));
                }
            } else if (Constants.User.Account.BALANCE.equalsIgnoreCase(accountStatus)
                    || Constants.User.Service.BALANCE.equalsIgnoreCase(serviceStatus)) {
                offlineFragment(Constants.User.Service.BALANCE);
                //imgStatus.setImageResource(R.drawable.banner_active);
            }
        }

        if (response.getRequests().isEmpty()) {
            CURRENT_DEST_ADDRESS = "";
            googleMap.clear();
            getDeviceLocation();
            changeFlow(Constants.checkStatus.EMPTY);
        } else {
            MvpApplication.time_to_left = response.getRequests().get(0).getTimeLeftToRespond();
            DATUM = response.getRequests().get(0).getRequest();
            changeFlow(DATUM.getStatus());
        }

        if (canGoToChatScreen) {
            if (!isChatScreenOpen && DATUM != null) {
                Intent i = new Intent(MainActivity.this, ChatActivity.class);
                i.putExtra(Constants.SharedPref.REQUEST_ID, String.valueOf(DATUM.getId()));
                startActivity(i);
            }
            canGoToChatScreen = false;
        }

    }

    @Override
    public void onSuccessProviderAvailable(Object object) {
        navInstantRide.setVisibility(View.GONE);
        Log.i("TTTTT", "onSuccessProviderAvailable");
        offlineFragment("");
        //sbChangeStatus.toggleState();
    }

    @Override
    public void onSuccessFCM(Object object) {
        Log.i("TTTTT", "onSuccessFCM");
        Utilities.printV("onSuccessFCM", "onSuccessFCM");
    }

    @Override
    public void onSuccessLocationUpdate(TripResponse tripResponse) {
        Log.i("TTTTT", "onSuccessLocationUpdate");
    }

    private AlertDialog mDialog;

    @Override
    public void onSuccessInstantNow(Object object) {
        HashMap<String, Object> params = new HashMap<>();
        if (mLastKnownLocation != null) {
            params.put("latitude", mLastKnownLocation.getLatitude());
            params.put("longitude", mLastKnownLocation.getLongitude());
        }

        hideLoading();
        mDialog.dismiss();

        presenter.getTrip(params);

        otpDialog.cancel();
    }

    @Override
    public void onSuccessInstant(EstimateFare object) {
        Log.i("TestEstimated", "Deu certo: Valor "+ object.getEstimatedFare());

        if (SharedHelper.getKey(this, "currency_code", null) == null) {
            StringBuilder stringBuilder = new StringBuilder(SharedHelper.getKey(this,
                    SharedHelper.CURRENCY));
            Log.i("TestEstimated", "If aqui");
            Log.i("TestEstimated", "If: "+ getString(R.string.estimate_fare_,
                    stringBuilder + numberFormat
                            .format(object.getEstimatedFare()).substring(1)));
            estimateFare.setText(getString(R.string.estimate_fare_,
                    stringBuilder + numberFormat
                            .format(object.getEstimatedFare()).substring(1)));
            Log.i("TestEstimated", "If - passou");
        } else {
            Log.i("TestEstimated", "Else");
            estimateFare.setText(getString(R.string.estimate_fare_,
                    numberFormat.format(object.getEstimatedFare())));
            Log.i("TestEstimated", "Else - passou");
        }
        Log.i("TestEstimated", "Saiu");
        timeTravel.setText(object.getTime());
        pinView.requestFocus();
        lnrVerification.setVisibility(View.VISIBLE);
        Log.i("TestEstimated", "Visible");
        llOTP.setVisibility(View.GONE);
        //server_otp = object.getOtp() + "";
        //user_id_now = object.getUser().getId() + "";


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_instant_ride, null);

        LatLng currentLatLng = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
        String address = getAddress(currentLatLng);

        TextView tvPickUp = view.findViewById(R.id.tvPickUp);
        TextView tvDrop = view.findViewById(R.id.tvDrop);
        TextView tvPhone = view.findViewById(R.id.tvPhone);
        TextView tvFare = view.findViewById(R.id.tvFare);

        tvPickUp.setText(address);
        tvDrop.setText(""+RIDE_REQUEST.get("d_address"));
        tvPhone.setText(numberPhone);
        tvFare.setText("R$"+ object.getEstimatedFare());

        builder.setView(view);
        mDialog = builder.create();
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Log.i("TestEstimated", "Passou");

        view.findViewById(R.id.tvSubmit).setOnClickListener(view1 -> {
            showLoading();

            HashMap<String, Object> map = new HashMap<>();
            map.put("s_latitude", mLastKnownLocation.getLatitude());
            map.put("s_longitude", mLastKnownLocation.getLongitude());
            map.put("s_address", address);
            map.put("d_latitude", RIDE_REQUEST.get("d_latitude"));
            map.put("d_longitude", RIDE_REQUEST.get("d_longitude"));
            map.put("d_address", RIDE_REQUEST.get("d_address"));
            map.put("user_id", user_id_now);
            Log.i("TestEstimated", "Entrou 2, Tipo: "+SharedHelper.getIntKey(this, Constants.SharedPref.SERVICE_TYPE));

            Log.i("TestLocation", "Teste - "+ address);

            presenter.instantRideSendRequest(map);
        });

        view.findViewById(R.id.tvCancel).setOnClickListener(view1 -> mDialog.dismiss());
        mDialog.show();
    }

    @Override
    public void onSuccess(EstimateFare estimateFare) {

    }

    @SuppressLint("StringFormatInvalid")
    public void changeFlow(String status) {
        Log.i("TTTTT", "changeFlow");
        System.out.println("RRR status = [" + status + "]");

        if (!DATUM.getDAddress().equalsIgnoreCase(CURRENT_DEST_ADDRESS)
                && STATUS.equalsIgnoreCase(Constants.checkStatus.PICKEDUP)
                && status.equalsIgnoreCase(Constants.checkStatus.PICKEDUP))
            showDestinationAlert(String.format(getString(R.string.destination_change_to), DATUM.getDAddress()));

        System.out.println("RRR getProviderId = " + "loc_p_" + DATUM.getProviderId());

        //SETA LATITUDE E LONGITUDE ATUAL
        if (googleMap != null) {
            Constants.CURRENT_LAT = googleMap.getMyLocation().getLatitude();
            Constants.CURRENT_LNG = googleMap.getMyLocation().getLongitude();
        }

        switch (status) {
            case Constants.checkStatus.EMPTY:
            case Constants.checkStatus.ACCEPTED:
            case STARTED:
            case ARRIVED:
            case Constants.checkStatus.PICKEDUP:
                String refPath = "loc_p_" + DATUM.getProviderId();
                if (!refPath.equals("loc_p_0") && mProviderLocation == null) {
                    mProviderLocation = FirebaseDatabase.getInstance().getReference()
                            .child("loc_p_" + DATUM.getProviderId());
                    updateDriverNavigation();
                }
                break;
            case Constants.checkStatus.DROPPED:
            case Constants.checkStatus.COMPLETED:
                mProviderLocation = null;
                break;
            default:
                mProviderLocation = null;
                break;
        }

        if (!STATUS.equalsIgnoreCase(status)) {
            STATUS = status;
            CURRENT_DEST_ADDRESS = DATUM.getDAddress();
            lblLocationType.setText(getString(R.string.pick_up_location));
            if (DATUM != null && DATUM.getSAddress() != null && !DATUM.getSAddress().isEmpty())
                lblLocationName.setText(DATUM.getSAddress());
            CURRENT_DEST_ADDRESS = DATUM.getDAddress();
            switch (status) {
                case Constants.checkStatus.EMPTY:
                    mProviderLocation = null;
                    changeFragment(null);
                    break;
                case Constants.checkStatus.SEARCHING:
                    changeFragment(new IncomingRequestFragment());
                    break;
                case Constants.checkStatus.ACCEPTED:
                    lnrLocationHeader.setVisibility(View.VISIBLE);
                    changeFragment(new StatusFlowFragment());
                    break;
                case STARTED:
                    lnrLocationHeader.setVisibility(View.VISIBLE);
                    changeFragment(new StatusFlowFragment());
                    break;
                case ARRIVED:
                    googleMap.clear();
                    lblLocationType.setText(getString(R.string.drop_off_location));
                    lblLocationName.setText(DATUM.getDAddress());
                    lnrLocationHeader.setVisibility(View.VISIBLE);
                    changeFragment(new StatusFlowFragment());
                    break;
                case Constants.checkStatus.PICKEDUP:
                    lblLocationType.setText(getString(R.string.drop_off_location));
                    lblLocationName.setText(DATUM.getDAddress());
                    lnrLocationHeader.setVisibility(View.VISIBLE);
                    changeFragment(new StatusFlowFragment());
                    break;
                case Constants.checkStatus.DROPPED:
                    lblLocationType.setText(getString(R.string.drop_off_location));
                    lblLocationName.setText(DATUM.getDAddress());
                    changeFragment(new InvoiceDialogFragment());
                    break;
                case Constants.checkStatus.COMPLETED:
                    lblLocationType.setText(getString(R.string.drop_off_location));
                    lblLocationName.setText(DATUM.getDAddress());
                    changeFragment(new RatingDialogFragment());
                    break;
                default:
                    break;
            }
        } else System.out.println("Opened Dialogs ==> " + hasOpenedDialogs());
    }

    private void updateDriverNavigation() {
        Log.i("TTTTT", "updateDriverNavigation");
        mProviderLocation.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    LatLngFireBaseDB fbData = dataSnapshot.getValue(LatLngFireBaseDB.class);
                    assert fbData != null;
                    double lat = fbData.getLat();
                    double lng = fbData.getLng();

                    System.out.println("RRR Lat: " + lat + " Lng: " + lng);


                    if (lng > 0 && lat > 0)
                        if (STARTED.equalsIgnoreCase(DATUM.getStatus()) ||
                                ARRIVED.equalsIgnoreCase(DATUM.getStatus()) ||
                                PICKEDUP.equalsIgnoreCase(DATUM.getStatus())) {
                            LatLng source = null, destination = null;
                            switch (DATUM.getStatus()) {
                                case STARTED:
                                    source = new LatLng(lat, lng);
                                    destination = new LatLng(DATUM.getSLatitude(), DATUM.getSLongitude());
                                    break;
                                case ARRIVED:
                                case PICKEDUP:
                                    source = new LatLng(lat, lng);
                                    destination = new LatLng(DATUM.getDLatitude(), DATUM.getDLongitude());
                                    break;
                            }
                            if (polyLinePoints == null || polyLinePoints.size() < 2 || mPolyline == null)
                                drawRoute(source, destination);
                            else {
                                int index = checkForReRoute(source);
                                if (index < 0) reRoutingDelay(source, destination);
                                else polyLineRerouting(source, index);
                            }

                            if (start != null) {
                                SharedHelper.putCurrentLocation(MainActivity.this, start);
                                end = start;
                            }
                            start = source;
                            if (end != null && canCarAnim) {
                                if (start != null && (start.latitude != end.latitude ||
                                        start.longitude != end.longitude))
                                    carAnim(srcMarker, end, start);
                            }
                        } else mProviderLocation = null;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("RRR ", "Failed to read value.", error.toException());
            }
        });
    }

    private void reRoutingDelay(LatLng source, LatLng destination) {
        Log.i("TTTTT", "reRoutingDelay");
        if (canReRoute) {
            canReRoute = !canReRoute;
            drawRoute(source, destination);
            new Handler().postDelayed(() -> canReRoute = true, 8000);
        }
    }

    private void polyLineRerouting(LatLng point, int index) {
        Log.i("TTTTT", "polyLineRerouting");
        if (index > 0) {
            polyLinePoints.subList(0, index + 1).clear();
            polyLinePoints.add(0, point);
            mPolyline.remove();

            mPolyline = googleMap.addPolyline(DirectionConverter.createPolyline
                    (this, polyLinePoints, 5, getResources().getColor(R.color.colorAccent)));

            System.out.println("RRR mPolyline = " + polyLinePoints.size());
        } else System.out.println("RRR mPolyline = Failed");
    }

    private int checkForReRoute(LatLng point) {
        Log.i("TTTTT", "checkForReRoute");
        if (polyLinePoints != null && polyLinePoints.size() > 0) {
            System.out.println("RRR indexOnEdgeOrPath = " +
                    new PolyUtil().indexOnEdgeOrPath(point, polyLinePoints, false, true, 100));
            //      indexOnEdgeOrPath returns -1 if the point is outside the polyline
            //      returns the index position if the point is inside the polyline
            return new PolyUtil().indexOnEdgeOrPath(point, polyLinePoints, false, true, 100);
        } else return -1;
    }

    public boolean hasOpenedDialogs() {
        Log.i("TTTTT", "hasOpenedDialogs");
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments)
            if (fragment != null && fragment.isVisible()) return true;
        return false;
    }

    private void showDestinationAlert(String message) {
        Log.i("TTTTT", "showDestinationAlert");
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(getString(R.string.ride_updated))
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.yes), (dialog, id) -> updateDestination());
            builder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void carAnim(final Marker marker, final LatLng start, final LatLng end) {
        Log.i("TTTTT", "carAnim");
        ValueAnimator animator = ValueAnimator.ofFloat(0, 1);
        animator.setDuration(1900);
        final LatLngInterface latLngInterpolator = new LatLngInterface.LinearFixed();
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(valueAnimator1 -> {
            try {
                canCarAnim = false;
                float v = valueAnimator1.getAnimatedFraction();
                LatLng newPos = latLngInterpolator.interpolate(v, start, end);
                marker.setPosition(newPos);
                marker.setAnchor(0.5f, 0.5f);
                marker.setRotation(bearingBetweenLocations(start, end));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                canCarAnim = true;
            }
        });
        animator.start();
    }

    private double rotateMarker(double currentLat, double currentLng,
                                double nextLat, double nextLng) {
        Log.i("TTTTT", "rotateMarker");
        double degToRad = Math.PI / 180.0;
        double phi1 = currentLat * degToRad;
        double phi2 = nextLat * degToRad;
        double lam1 = currentLng * degToRad;
        double lam2 = nextLng * degToRad;

        return Math.atan2(
                Math.sin(lam2 - lam1) * Math.cos(phi2),
                Math.cos(phi1) * Math.sin(phi2) - Math.sin(phi1)
                        * Math.cos(phi2) * Math.cos(lam2 - lam1))
                * 180 / Math.PI;
    }

    private void updateDestination() {
        Log.i("TTTTT", "updateDestination");
        STATUS = "";
        if (googleMap != null) googleMap.clear();
    }

    public void ShowLogoutPopUp() {
        Log.i("TTTTT", "ShowLogoutPopUp");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setMessage(getString(R.string.log_out_title))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), (dialog, id) -> {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("id", SharedHelper.getKey(activity(),
                            Constants.SharedPref.USER_ID) + "");
                    presenter.logout(map);
                }).setNegativeButton(getString(R.string.cancel), (dialog, id) -> {
            String user_id = SharedHelper.getKey(activity(), Constants.SharedPref.USER_ID);
            Utilities.printV("user_id===>", user_id);
            dialog.cancel();
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void drawRoute(LatLng source, LatLng destination) {
        Log.i("TTTTT", "drawRoute");
        GoogleDirection.withServerKey(getResources().getString(R.string.google_maps_key))
                .from(source)
                .to(destination)
                .transportMode(TransportMode.DRIVING)
                .execute(this);
    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        Log.i("TTTTT", "onDirectionSuccess");
        if (direction.isOK()) {
            googleMap.clear();
            Route route = direction.getRouteList().get(0);
            if (!route.getLegList().isEmpty()) {

//                Leg startLeg = route.getLegList().get(0);
                Leg endLeg = route.getLegList().get(0);

//                LatLng origin = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                LatLng destination = new LatLng(endLeg.getEndLocation().getLatitude(), endLeg.getEndLocation().getLongitude());

                //SETA TIPO DE ICONES
//                Integer serviceIcon;
//                if(DATUM != null){
//
//                    if(DATUM.getServiceTypeId() == serviceMototaxiID){
//                        //Se for mototaxi
//                        serviceIcon = R.drawable.moto_icon_2;
//                    }else if(DATUM.getServiceTypeId() == serviceMotoboyID){
//                        //Se for motoboy
//                        serviceIcon = R.drawable.moto_icon_2;
//                    }else{
//                        //Se for outro (carro)
//                        serviceIcon = R.drawable.car_icon_1;
//                    }
//                }else{
//                    serviceIcon = R.drawable.car_icon_1;
//                }

//                srcMarker = googleMap.addMarker(new MarkerOptions()
//                        .position(origin)
//                        .icon(BitmapDescriptorFactory.fromResource(serviceIcon)));
                googleMap.addMarker(new MarkerOptions()
                        .position(destination)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.des_icon)))
                        .setTag(endLeg);
            }

            polyLinePoints = route.getLegList().get(0).getDirectionPoint();
            mPolyline = googleMap.addPolyline(DirectionConverter.createPolyline
                    (this, polyLinePoints, 5, getResources().getColor(R.color.colorAccent)));
            setCameraWithCoordinationBounds(route);

        } else Toast.makeText(this, direction.getStatus(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onDirectionFailure(Throwable t) {
        Log.i("TTTTT", "onDirectionFailure");
        Toast.makeText(this, t.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void setCameraWithCoordinationBounds(Route route) {
        Log.i("TTTTT", "setCameraWithCoordinationBounds");
        LatLng southwest = route.getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.getBound().getNortheastCoordination().getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }

    public void navigateToShareScreen() {
        Log.i("TTTTT", "navigateToShareScreen");
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text) + "\n" +
                getString(R.string.share_link));
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public float bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {
        Log.i("TTTTT", "bearingBetweenLocations");
        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        return (float) brng;
    }

    private interface LatLngInterface {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterface {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                if (Math.abs(lngDelta) > 180) lngDelta -= Math.signum(lngDelta) * 360;
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

}
