package com.palmdrivermotorista.driver.ui.fragment.upcoming;


import com.palmdrivermotorista.driver.base.MvpPresenter;

public interface UpcomingTripIPresenter<V extends UpcomingTripIView> extends MvpPresenter<V> {

    void getUpcoming();

}
