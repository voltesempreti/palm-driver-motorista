package com.palmdrivermotorista.driver.ui.activity.profile;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.palmdrivermotorista.driver.common.validator.ValidaCPF;
import com.palmdrivermotorista.driver.BuildConfig;
import com.palmdrivermotorista.driver.MvpApplication;
import com.palmdrivermotorista.driver.R;
import com.palmdrivermotorista.driver.base.BaseActivity;
import com.palmdrivermotorista.driver.common.Constants;
import com.palmdrivermotorista.driver.common.SharedHelper;
import com.palmdrivermotorista.driver.common.Utilities;
import com.palmdrivermotorista.driver.data.network.model.UserResponse;
import com.palmdrivermotorista.driver.ui.activity.profile_update.ProfileUpdateActivity;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class ProfileActivity extends BaseActivity implements ProfileIView {

    private ProfilePresenter presenter = new ProfilePresenter();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imgProfile)
    CircularImageView imgProfile;
    @BindView(R.id.completeData)
    LinearLayout completeData;
    @BindView(R.id.nameProfile)
    TextView txtName;
    @BindView(R.id.cpfProfile)
    TextView txtCpf;
    @BindView(R.id.mobileProfile)
    TextView txtPhoneNumber;
    @BindView(R.id.emailProfile)
    TextView txtEmail;
    @BindView(R.id.serviceProfile)
    TextView txtService;
    @BindView(R.id.modelVeicleProfile)
    TextView txtModel;
    @BindView(R.id.boardProfile)
    TextView txtNumber;
    //@BindView(R.id.lblChangePassword)
    //TextView lblChangePassword;
    @BindView(R.id.fabEdit)
    FloatingActionButton edit;

    private File imgFile = null;
    private String qrCodeUrl;
    private AlertDialog mDialog;
    private ValidaCPF validaCPF;

    @Override
    public int getLayoutId() {
        return R.layout.activity_profile;

    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        presenter.attachView(this);
        showLoading();
        presenter.getProfile();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.profile));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick({R.id.fabEdit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fabEdit:
                Intent intent = new Intent(ProfileActivity.this, ProfileUpdateActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onSuccess(UserResponse user) {
        hideLoading();
        Utilities.printV("User===>", user.getFirstName() + user.getLastName());
        Utilities.printV("TOKEN===>", SharedHelper.getKey(MvpApplication.getInstance(),
                Constants.SharedPref.ACCESS_TOKEN, ""));

        String loginBy = user.getLoginBy();

        //Verifica cadastro incompleto
        if(Constants.userCpf == null || Constants.userAvatar == null){
            completeData.setVisibility(View.VISIBLE);
        }

        //if (loginBy.equalsIgnoreCase("facebook") || loginBy.equalsIgnoreCase("google"))
        //    lblChangePassword.setVisibility(View.INVISIBLE);
        //else lblChangePassword.setVisibility(View.VISIBLE);

        txtName.setText(user.getFirstName() + " " + user.getLastName());
        txtCpf.setText("CPF: "+user.getCpf());
        txtPhoneNumber.setText("Número: " + String.valueOf(user.getMobile()));
        txtNumber.setText("Placa do Veículo: " + user.getService().getServiceNumber());
        txtModel.setText("Modelo do Veículo: " + user.getService().getServiceModel());

        txtEmail.setText(user.getEmail());
        SharedHelper.putKey(this, Constants.SharedPref.STRIPE_PUBLISHABLE_KEY, user.getStripePublishableKey());
        if (user.getService() != null)
            txtService.setText(("Serviço: "+ user.getService().getServiceType() != null)
                    ? "Serviço: "+ user.getService().getServiceType().getName() : "");
        Glide.with(activity())
                .load(BuildConfig.BASE_IMAGE_URL + user.getAvatar())
                .apply(RequestOptions
                        .placeholderOf(R.drawable.ic_user_placeholder)
                        .dontAnimate()
                        .error(R.drawable.ic_user_placeholder))
                .into(imgProfile);
        Constants.showOTP = user.getRide_otp().equals("1");
        Constants.showTOLL = user.getRide_toll().equals("1");
        qrCodeUrl = !TextUtils.isEmpty(user.getQrcode_url()) ? BuildConfig.BASE_URL + user.getQrcode_url() : null;
        }

    @Override
    public void onError(Throwable e) {
        hideLoading();
        if (e != null)
            onErrorBase(e);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, ProfileActivity.this,
                new DefaultCallback() {
                    @Override
                    public void onImagesPicked(@NonNull List<File> imageFiles,
                                               EasyImage.ImageSource source, int type) {
                        imgFile = imageFiles.get(0);
                        Glide.with(activity())
                                .load(Uri.fromFile(imgFile))
                                .apply(RequestOptions
                                        .placeholderOf(R.drawable.ic_user_placeholder)
                                        .dontAnimate()
                                        .error(R.drawable.ic_user_placeholder))
                                .into(imgProfile);
                    }
                });
    }

}
