package com.palmdrivermotorista.driver.ui.bottomsheetdialog.invoice_flow;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.palmdrivermotorista.driver.MvpApplication;
import com.palmdrivermotorista.driver.R;
import com.palmdrivermotorista.driver.base.BaseFragment;
import com.palmdrivermotorista.driver.common.Constants;
import com.palmdrivermotorista.driver.common.mask.MoneyTextWatcher;
import com.palmdrivermotorista.driver.data.network.model.Request_;

import androidx.fragment.app.Fragment;
import android.widget.Toast;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;

import static com.palmdrivermotorista.driver.MvpApplication.DATUM;

public class InvoiceDialogFragment extends BaseFragment implements InvoiceDialogIView {

    @BindView(R.id.promotion_amount)
    TextView promotionAmount;
    @BindView(R.id.wallet_amount)
    TextView walletAmount;
    @BindView(R.id.booking_id)
    TextView bookingId;
//    @BindView(R.id.total_amount)
//    TextView totalAmount;
    @BindView(R.id.payable_amount)
    TextView payableAmount;
    @BindView(R.id.payment_mode_img)
    ImageView paymentModeImg;
    @BindView(R.id.payment_mode_layout)
    LinearLayout paymentModeLayout;
    @BindView(R.id.llAmountToBePaid)
    LinearLayout llAmountToBePaid;
    Unbinder unbinder;
    @BindView(R.id.btnConfirmPayment)
    Button btnConfirmPayment;
    @BindView(R.id.btnEnableVirtualChange)
    Button btnEnableVirtualChange;
    @BindView(R.id.lnrVirtualChange)
    LinearLayout lnrVirtualChange;
    @BindView(R.id.change_value)
    EditText changeValue;

    InvoiceDialogPresenter presenter;
    @BindView(R.id.lblPaymentType)
    TextView lblPaymentType;

    private boolean virtualChangeEnabled = false;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_invoice_dialog;
    }

    @Override
    public Fragment fragmentInstance() {
        return InvoiceDialogFragment.this;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View initView(View view) {
        unbinder = ButterKnife.bind(this, view);
        presenter = new InvoiceDialogPresenter();
        presenter.attachView(this);
        // setCancelable(false);

        Locale mLocale = new Locale("pt", "BR");
        changeValue.addTextChangedListener(new MoneyTextWatcher(changeValue, mLocale));

        if (DATUM != null) {
            Request_ datum = DATUM;
            bookingId.setText(datum.getBookingId());

            //TODO ALLAN - Alterações débito na máquina e voucher
            if(datum.getPaymentMode().equals("CASH")){
                lblPaymentType.setText("DINHEIRO");
                btnEnableVirtualChange.setVisibility(View.VISIBLE);
            }else if(datum.getPaymentMode().equals("DEBIT_MACHINE")){
                lblPaymentType.setText("DÉBITO NA MÁQUINA");
                paymentModeImg.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_debit_machine));
            }else if(datum.getPaymentMode().equals("VOUCHER")) {
                lblPaymentType.setText("VOUCHER");
                paymentModeImg.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_voucher));
            }else{
                lblPaymentType.setText("CARTÃO");
                paymentModeImg.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_card));
            }

            if (datum.getPayment() != null) {
                if (datum.getPayment().getPayable() > 0) {
                    llAmountToBePaid.setVisibility(View.VISIBLE);

                    //Se for pagamento co cartão exibe valor descontando as taxas
                    if(datum.getPaymentMode().equals("CARD")){
                        payableAmount.setText(Constants.Currency +
                                MvpApplication.getInstance().getNewNumberFormat(Double.parseDouble(datum.getPayment().getProviderPay() + "")));
                    }else{
                        payableAmount.setText(Constants.Currency +
                                MvpApplication.getInstance().getNewNumberFormat(Double.parseDouble(datum.getPayment().getPayable() + "")));
                    }
                } else llAmountToBePaid.setVisibility(View.GONE);
            }else{
                Toasty.info(this.getContext(), "Falha ao exibir dados! Por favor, minimize o aplicativo e abra novamente.", Toast.LENGTH_LONG).show();
                payableAmount.setText("Falha");
            }
        }
        return view;
    }

    @Override
    public void onSuccess(Object message) {
        hideLoading();
        activity().sendBroadcast(new Intent("INTENT_FILTER"));

    }


    @Override
    public void onError(Throwable e) {
        hideLoading();

        try {
            if (e != null)
                onErrorBase(e);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

    }

    @OnClick({R.id.btnConfirmPayment, R.id.btnEnableVirtualChange})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.btnConfirmPayment:

                if (DATUM != null) {
                    //TODO ALLAN - Alterações débito na máquina e voucher
                    if(DATUM.getPaymentMode().equals("CARD")){
                        Request_ datum = DATUM;
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("status", Constants.checkStatus.COMPLETED);
                        map.put("request_id", datum.getId());
                        map.put("tips", 0.0);
                        map.put("payment_type", Constants.PaymentMode.CARD);
                        map.put("payment_mode", Constants.PaymentMode.CARD);
                        map.put("_method", Constants.checkStatus.PATCH);
                        showLoading();
                        presenter.statusUpdate(map, datum.getId());
                    }else if(DATUM.getPaymentMode().equals("DEBIT_MACHINE")){
                        Request_ datum = DATUM;
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("status", Constants.checkStatus.COMPLETED);
                        map.put("payment_mode", "DEBIT_MACHINE");
                        map.put("_method", Constants.checkStatus.PATCH);
                        showLoading();
                        presenter.statusUpdate(map, datum.getId());
                        presenter.statusUpdate(map, datum.getId());
                    }else if(DATUM.getPaymentMode().equals("VOUCHER")){
                        Request_ datum = DATUM;
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("status", Constants.checkStatus.COMPLETED);
                        map.put("payment_mode", "VOUCHER");
                        map.put("_method", Constants.checkStatus.PATCH);
                        showLoading();
                        presenter.statusUpdate(map, datum.getId());
                    }else{
                        if(!virtualChangeEnabled){
                            Request_ datum = DATUM;
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("status", Constants.checkStatus.COMPLETED);
                            map.put("payment_mode", "CASH");
                            map.put("_method", Constants.checkStatus.PATCH);
                            showLoading();
                            presenter.statusUpdate(map, datum.getId());
                        } else{
                            if(changeValue.getText().toString().equals("")){
                                Toast.makeText(getContext(), "Informe o valor do troco!", Toast.LENGTH_SHORT).show();
                            } else{
                                String stringValue = changeValue.getText().toString().replaceAll("R", "").replaceAll("\\$", "").replaceAll("\\s+", "").replaceAll(",", ".");
                                double value = Double.parseDouble(stringValue);
                                if(value > 20){
                                    Toast.makeText(getContext(), "O valor máximo para troco é de R$20,00!", Toast.LENGTH_SHORT).show();
                                } else{
                                    Request_ datum = DATUM;
                                    HashMap<String, Object> map = new HashMap<>();
                                    map.put("status", Constants.checkStatus.COMPLETED);
                                    map.put("payment_mode", "CASH");
                                    map.put("virtual_change", value);
                                    map.put("_method", Constants.checkStatus.PATCH);
                                    showLoading();
                                    presenter.statusUpdate(map, datum.getId());
                                }

                            }
                        }
                    }
                }
                break;

            case R.id.btnEnableVirtualChange:

                if(!virtualChangeEnabled){
                    btnEnableVirtualChange.setText(R.string.desabilitar_troco_virtual);
                    lnrVirtualChange.setVisibility(View.VISIBLE);
                    virtualChangeEnabled = true;
                } else{
                    btnEnableVirtualChange.setText(R.string.troco_virtual);
                    lnrVirtualChange.setVisibility(View.GONE);
                    virtualChangeEnabled = false;
                }

                break;

            default:
                break;
        }
    }

    public void ShowPaymentPopUp() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.getContext());
        alertDialogBuilder
//                .setMessage(getString(R.string.log_out_title))
                .setMessage("ATENÇÃO! O passageiro socilicitou o pagamento com cartão de crédito, aguarde o passageiro. Confirme informando que recebeu o pagamento.")
                .setCancelable(false)
                .setPositiveButton("Confirmar", (dialog, id) -> {
                    Request_ datum = DATUM;
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("status", Constants.checkStatus.COMPLETED);
                    map.put("payment_mode", "CASH");
                    map.put("_method", Constants.checkStatus.PATCH);
                    showLoading();
                    presenter.statusUpdate(map, datum.getId());
                }).setNegativeButton(getString(R.string.cancel), (dialog, id) -> {
            dialog.cancel();
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}
