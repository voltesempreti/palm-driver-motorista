package com.palmdrivermotorista.driver.ui.activity.register;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.provider.Settings;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.palmdrivermotorista.driver.common.mask.MaskEditUtil;
import com.palmdrivermotorista.driver.common.validator.ValidaCPF;
import com.google.firebase.iid.FirebaseInstanceId;
import com.palmdrivermotorista.driver.BuildConfig;
import com.palmdrivermotorista.driver.R;
import com.palmdrivermotorista.driver.base.BaseActivity;
import com.palmdrivermotorista.driver.common.Constants;
import com.palmdrivermotorista.driver.common.SharedHelper;
import com.palmdrivermotorista.driver.common.Utilities;
import com.palmdrivermotorista.driver.data.network.model.City;
import com.palmdrivermotorista.driver.data.network.model.ServiceType;
import com.palmdrivermotorista.driver.data.network.model.SettingsResponse;
import com.palmdrivermotorista.driver.data.network.model.State;
import com.palmdrivermotorista.driver.data.network.model.User;
import com.palmdrivermotorista.driver.ui.activity.main.MainActivity;
import com.palmdrivermotorista.driver.ui.countrypicker.Country;
import com.palmdrivermotorista.driver.ui.countrypicker.CountryPicker;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RegisterActivity extends BaseActivity implements RegisterIView {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtEmail)
    EditText txtEmail;
    @BindView(R.id.txtFirstName)
    EditText txtFirstName;
    @BindView(R.id.txtLastName)
    EditText txtLastName;
    @BindView(R.id.txtCpf)
    EditText txtCpf;
    @BindView(R.id.txtPassword)
    EditText txtPassword;
    @BindView(R.id.txtConfirmPassword)
    EditText txtConfirmPassword;
    @BindView(R.id.chkTerms)
    CheckBox chkTerms;
    @BindView(R.id.spinnerServiceType)
    AppCompatSpinner spinnerServiceType;
    @BindView(R.id.txtVehicleModel)
    EditText txtVehicleModel;
    @BindView(R.id.txtVehicleNumber)
    EditText txtVehicleNumber;
    @BindView(R.id.lnrReferralCode)
    LinearLayout lnrReferralCode;
    @BindView(R.id.txtReferalCode)
    EditText txtReferalCode;
    @BindView(R.id.countryImage)
    ImageView countryImage;
    @BindView(R.id.countryNumber)
    TextView countryNumber;
    @BindView(R.id.phoneNumber)
    EditText phoneNumber;
    @BindView(R.id.state_spinner)
    AppCompatSpinner state_Spinner;
    @BindView(R.id.city_spinner)
    AppCompatSpinner city_Spinner;
    @BindView(R.id.sexy_spinner)
    AppCompatSpinner sexy_Spinner;

    private String countryDialCode = "+55";
    private String countryCode = "BR";
    private CountryPicker mCountryPicker;
    private RegisterPresenter presenter;
    private int selected_pos = -1;
    private List<ServiceType> lstServiceTypes = new ArrayList<>();
    private List<City> lstCities = new ArrayList<>();
    private ArrayAdapter<State> stateArrayAdapter;
    private ArrayAdapter<City> cityArrayAdapter;
    private ArrayAdapter<String> dataAdapter;
    private ValidaCPF validaCPF;
    private boolean isEmailAvailable = true;
    private boolean isPhoneNumberAvailable = true;

    @Override
    public int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        presenter = new RegisterPresenter();
        presenter.attachView(this);
        presenter.getStates();

        //Mascaras de CPF e Telefone//
        txtCpf.addTextChangedListener(MaskEditUtil.mask(txtCpf, MaskEditUtil.FORMAT_CPF));
        phoneNumber.addTextChangedListener(MaskEditUtil.mask(phoneNumber, MaskEditUtil.FORMAT_FONE));
        //Fim Mascaras de CPF e Telefone//

        city_Spinner.setEnabled(false);
        setupSpinnerCities();
        spinnerServiceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_pos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

        clickFunctions();

        setCountryList();

        if (BuildConfig.DEBUG) {
            txtEmail.setText("");
            txtFirstName.setText("");
            txtLastName.setText("");
            txtVehicleModel.setText("");
            txtVehicleNumber.setText("");
            phoneNumber.setText("");
            txtPassword.setText("");
            txtConfirmPassword.setText("");
        }

        if (SharedHelper.getKey(this, Constants.SharedPref.DEVICE_TOKEN).isEmpty()) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
                if (!task.isSuccessful()) {
                    Log.w("PasswordActivity", "getInstanceId failed", task.getException());
                    return;
                }
                Log.d("FCM_TOKEN", task.getResult().getToken());

                SharedHelper.putKey(RegisterActivity.this, Constants.SharedPref.DEVICE_TOKEN, task.getResult().getToken());
            });
        }

        @SuppressLint("HardwareIds")
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        SharedHelper.putKeyFCM(RegisterActivity.this, Constants.SharedPref.DEVICE_ID, deviceId);
    }

    private AdapterView.OnItemSelectedListener state_listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position > 0) {
                showLoading();
                if(dataAdapter != null){
                    dataAdapter.clear();
                    dataAdapter.notifyDataSetChanged();
                }
                final State state = (State) state_Spinner.getItemAtPosition(position);
                presenter.getCities(state.getId());
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private AdapterView.OnItemSelectedListener city_listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(position > 0){
                presenter.getServices(lstCities.get(position).getId());
                setupSpinner(null);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    public City findUsingCityForLoop(
            String name, List<City> cities) {

        for (City city : cities) {
            if (city.getTitle().equals(name)) {
                return city;
            }
        }
        return null;
    }

    private void setCountryList() {
        mCountryPicker = CountryPicker.newInstance("Select Country");
        List<Country> countryList = Country.getAllCountries();
        Collections.sort(countryList, (s1, s2) -> s1.getName().compareToIgnoreCase(s2.getName()));
        mCountryPicker.setCountriesList(countryList);

        setListener();
    }

    private void setListener() {
        mCountryPicker.setListener((name, code, dialCode, flagDrawableResID) -> {
            countryNumber.setText(dialCode);
            countryDialCode = dialCode;
            countryImage.setImageResource(flagDrawableResID);
            mCountryPicker.dismiss();
        });

        countryImage.setOnClickListener(v -> mCountryPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER"));
        countryNumber.setOnClickListener(v -> mCountryPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER"));

        getUserCountryInfo();
    }

    private void getUserCountryInfo() {
        Country country = getDeviceCountry(RegisterActivity.this);
        countryImage.setImageResource(country.getFlag());
        countryNumber.setText(country.getDialCode());
        countryDialCode = country.getDialCode();
        countryCode = country.getCode();
    }

    private boolean validation() {
        validaCPF = new ValidaCPF();
        boolean valida = validaCPF.isCPF(txtCpf.getText().toString().replaceAll("\\.", "").replace("-", ""));
        if (!valida){
            Toast.makeText(this, "CPF inválido!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (txtCpf.getText().toString().trim().isEmpty()) {
            Toasty.error(this, "Por favor, informe seu CPF", Toast.LENGTH_SHORT, true).show();
            return false;
        } else if (txtEmail.getText().toString().isEmpty()) {
            Toasty.error(this, getString(R.string.invalid_email), Toast.LENGTH_SHORT, true).show();
            return false;
        } else if (txtFirstName.getText().toString().trim().isEmpty()) {
            Toasty.error(this, getString(R.string.invalid_first_name), Toast.LENGTH_SHORT, true).show();
            return false;
        } else if (txtLastName.getText().toString().trim().isEmpty()) {
            Toasty.error(this, getString(R.string.invalid_last_name), Toast.LENGTH_SHORT, true).show();
            return false;
        } else if (state_Spinner.getSelectedItemId() == 0) {
            Toasty.error(this, getString(R.string.invalid_state), Toast.LENGTH_SHORT).show();
            return false;
        } else if (city_Spinner.getSelectedItemId() == 0) {
            Toasty.error(this, getString(R.string.invalid_city), Toast.LENGTH_SHORT).show();
            return false;
        } else if (selected_pos == -1 || dataAdapter.isEmpty()) {
            Toasty.error(this, getString(R.string.invalid_service_type), Toast.LENGTH_SHORT, true).show();
            return false;
        } else if (txtVehicleModel.getText().toString().trim().isEmpty()) {
            Toasty.error(this, getString(R.string.invalid_car_model), Toast.LENGTH_SHORT, true).show();
            return false;
        } else if (txtVehicleNumber.getText().toString().trim().isEmpty()) {
            Toasty.error(this, getString(R.string.invalid_car_number), Toast.LENGTH_SHORT, true).show();
            return false;
        } else if (phoneNumber.getText().toString().trim().isEmpty()) {
            Toasty.error(this, getString(R.string.invalid_phone_number), Toast.LENGTH_SHORT).show();
            return false;
        } else if (txtPassword.getText().toString().length() < 6) {
            Toasty.error(this, getString(R.string.invalid_password_length), Toast.LENGTH_SHORT, true).show();
            return false;
        } else if (txtConfirmPassword.getText().toString().trim().isEmpty()) {
            Toasty.error(this, getString(R.string.invalid_confirm_password), Toast.LENGTH_SHORT, true).show();
            return false;
        } else if (!txtPassword.getText().toString().equals(txtConfirmPassword.getText().toString())) {
            Toasty.error(this, getString(R.string.password_should_be_same), Toast.LENGTH_SHORT, true).show();
            return false;
        } else if (!chkTerms.isChecked()) {
            Toasty.error(this, getString(R.string.please_accept_terms_conditions), Toast.LENGTH_SHORT, true).show();
            return false;
        }  else return true;
    }

    private void showErrorMessage(EditText view, String message) {
        Toasty.error(this, message, Toast.LENGTH_SHORT).show();
        view.requestFocus();
        view.setText(null);
    }

    private void register(String countryCode, String phoneNumber) {

        City cityValue = this.findUsingCityForLoop(city_Spinner.getSelectedItem().toString(), lstCities);
        Log.d("POSICAO==>", Integer.toString(selected_pos));

        Map<String, RequestBody> map = new HashMap<>();
        map.put("first_name", toRequestBody(txtFirstName.getText().toString()));
        map.put("last_name", toRequestBody(txtLastName.getText().toString()));
        map.put("cpf", toRequestBody(txtCpf.getText().toString()));
        map.put("email", toRequestBody(txtEmail.getText().toString()));
        map.put("mobile", toRequestBody(phoneNumber));
        map.put("gender", toRequestBody(sexy_Spinner.getSelectedItem().toString()));
        map.put("country_code", toRequestBody(countryCode));
        map.put("city_id", toRequestBody(Integer.toString(cityValue.getId())));
        map.put("password", toRequestBody(txtPassword.getText().toString()));
        map.put("password_confirmation", toRequestBody(txtConfirmPassword.getText().toString()));
        map.put("device_token", toRequestBody(SharedHelper.getKeyFCM(this, Constants.SharedPref.DEVICE_TOKEN)));
        map.put("device_id", toRequestBody(SharedHelper.getKeyFCM(this, Constants.SharedPref.DEVICE_ID)));
        map.put("service_type", toRequestBody(Integer.toString(lstServiceTypes.get(selected_pos).getId())));
        map.put("service_model", toRequestBody(txtVehicleModel.getText().toString()));
        map.put("service_number", toRequestBody(txtVehicleNumber.getText().toString()));
        map.put("device_type", toRequestBody(BuildConfig.DEVICE_TYPE));
        map.put("referral_unique_id", toRequestBody(txtReferalCode.getText().toString()));

        Log.i("TestId", ""+Integer.toString(lstServiceTypes.get(selected_pos).getId()));

        List<MultipartBody.Part> parts = new ArrayList<>();
        showLoading();
        presenter.register(map, parts);

    }

    @OnClick({R.id.next, R.id.back, R.id.lblTerms})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.next:
                if (validation()) if (Utilities.isConnected()){
                    register(SharedHelper.getKey(RegisterActivity.this, Constants.SharedPref.DIAL_CODE),phoneNumber.getText().toString());
                }
                //fbPhoneLogin(countryCode, countryDialCode, phoneNumber.getText().toString());
                else showAToast(getString(R.string.no_internet_connection));
                break;
            case R.id.lblTerms:
                showTermsConditionsDialog();
                break;
            case R.id.back:
                onBackPressed();
                break;
        }
    }

    private void clickFunctions() {
        txtEmail.setOnFocusChangeListener((v, hasFocus) -> {
            isEmailAvailable = true;
            if (!hasFocus && !TextUtils.isEmpty(txtEmail.getText().toString()))
                if (Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText().toString()).matches())
                    presenter.verifyEmail(txtEmail.getText().toString().trim());
        });
    }

    private void showTermsConditionsDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getText(R.string.terms_and_conditions));
        WebView wv = new WebView(this);
        wv.loadUrl(BuildConfig.TERMS_CONDITIONS);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        alert.setView(wv);
        alert.setNegativeButton("Close", (dialog, id) -> dialog.dismiss());
        alert.show();
    }

    @Override
    public void onSuccess(User user) {
        hideLoading();
        SharedHelper.putKey(this, Constants.SharedPref.USER_ID, String.valueOf(user.getId()));
        SharedHelper.putKey(this, Constants.SharedPref.ACCESS_TOKEN, user.getAccessToken());
        SharedHelper.putKey(this, Constants.SharedPref.LOGGGED_IN, "true");
        Toasty.success(this, getString(R.string.register_success), Toast.LENGTH_SHORT, true).show();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void onSuccess(Object verifyEmail) {
        hideLoading();
        isEmailAvailable = false;
    }

    @Override
    public void onVerifyEmailError(Throwable e) {
        isEmailAvailable = true;
        showErrorMessage(txtEmail, getString(R.string.email_already_exist));
    }

    @Override
    public void onSuccessStates(List<State> states) {

        ArrayList<State> arrayStates = new ArrayList<>();

        arrayStates.add(new State(0,"Selecione um estado"));
        for (State state: states){
            arrayStates.add(new State(state.getId(), state.getTitle()));
        }

        stateArrayAdapter = new ArrayAdapter<State>(getApplicationContext(), R.layout.spinner, arrayStates);
        stateArrayAdapter.setDropDownViewResource(R.layout.spinner);
        state_Spinner.setAdapter(stateArrayAdapter);
    }

    @Override
    public void onSuccessCities(List<City> cities) {
        hideLoading();
        lstCities.clear();
        lstCities.add(new City(0,null,"Selecione a cidade"));
        for (City singleCity : cities) {
            lstCities.add(new City(singleCity.getId(),singleCity.getState(),singleCity.getTitle()));
        }

        cityArrayAdapter = new ArrayAdapter<City>(getApplicationContext(), R.layout.spinner, lstCities);
        cityArrayAdapter.setDropDownViewResource(R.layout.spinner);
        city_Spinner.setAdapter(cityArrayAdapter);
        city_Spinner.setEnabled(true);
    }

    @Override
    public void onSuccess(SettingsResponse response) {
        lstServiceTypes = response.getServiceTypes();
        lnrReferralCode.setVisibility(response.getReferral().getReferral().equalsIgnoreCase("1") ? View.VISIBLE : View.GONE);
        setupSpinner(response);
    }

    private void setupSpinner(@Nullable SettingsResponse response) {
        ArrayList<String> lstNames = new ArrayList<>();
        if (response != null){
            //lstNames.add("Selecione o serviço");
            for (ServiceType serviceType : response.getServiceTypes())
                lstNames.add(serviceType.getName());
        }
        dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner, lstNames);
        dataAdapter.setDropDownViewResource(R.layout.spinner);
        spinnerServiceType.setAdapter(dataAdapter);

    }

    private void setupSpinnerCities(){

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.gender_list, R.layout.spinner);
        adapter.setDropDownViewResource(R.layout.spinner);
        sexy_Spinner.setAdapter(adapter);

        state_Spinner.setOnItemSelectedListener(state_listener);
        city_Spinner.setOnItemSelectedListener(city_listener);
    }

    @Override
    public void onError(Throwable e) {
        hideLoading();
        if (e != null)
            onErrorBase(e);
    }

    @Override
    public void onSuccessPhoneNumber(Object object) {
        isPhoneNumberAvailable = false;
    }

    @Override
    public void onVerifyPhoneNumberError(Throwable e) {
        isPhoneNumberAvailable = true;
        showErrorMessage(phoneNumber, getString(R.string.mobile_number_already_exist));
    }
}