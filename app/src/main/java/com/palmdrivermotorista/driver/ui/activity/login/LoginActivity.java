package com.palmdrivermotorista.driver.ui.activity.login;

import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.palmdrivermotorista.driver.BuildConfig;
import com.palmdrivermotorista.driver.R;
import com.palmdrivermotorista.driver.base.BaseActivity;
import com.palmdrivermotorista.driver.common.Constants;
import com.palmdrivermotorista.driver.common.SharedHelper;
import com.palmdrivermotorista.driver.data.network.model.ForgotResponse;
import com.palmdrivermotorista.driver.data.network.model.User;
import com.palmdrivermotorista.driver.ui.activity.main.MainActivity;
import com.palmdrivermotorista.driver.ui.activity.password.PasswordIView;
import com.palmdrivermotorista.driver.ui.activity.password.PasswordPresenter;
import com.palmdrivermotorista.driver.ui.activity.register.RegisterActivity;
import com.palmdrivermotorista.driver.ui.activity.reset_password.ResetActivity;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class LoginActivity extends BaseActivity implements PasswordIView {

    @BindView(R.id.login_email)
    EditText email;
    @BindView(R.id.login_password)
    EditText password;
    @BindView(R.id.bt_login)
    Button login;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    public static String TAG = "";
    PasswordPresenter presenter = new PasswordPresenter();

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mToolbar.setNavigationOnClickListener(v -> finish());

        presenter.attachView(this);

    }

    @OnClick({R.id.bt_login, R.id.forgot_password, R.id.sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.sign_up:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
            case R.id.forgot_password:
                showLoading();
                showLoading();
                HashMap<String, Object> map = new HashMap<>();
                map.put("email", email.getText().toString());
                presenter.forgot(map);
                break;
            case R.id.bt_login:
                if(validate())
                    login();
                break;
        }
    }

    private void login() {

        deviceToken = SharedHelper.getKeyFCM(this, Constants.SharedPref.DEVICE_TOKEN);
        deviceId = SharedHelper.getKeyFCM(this, Constants.SharedPref.DEVICE_ID);

        if (TextUtils.isEmpty(deviceToken))
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    deviceToken = instanceIdResult.getToken();
                    SharedHelper.putKeyFCM(LoginActivity.this, Constants.SharedPref.DEVICE_TOKEN, deviceToken);
                }
            });

        if (TextUtils.isEmpty(deviceId)) {
            deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            SharedHelper.putKeyFCM(this, Constants.SharedPref.DEVICE_ID, deviceId);
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("email", email.getText().toString());
        map.put("password", password.getText().toString());
        map.put("device_id", deviceId);
        map.put("device_type", BuildConfig.DEVICE_TYPE);
        map.put("device_token", deviceToken);
        presenter.login(map);
        showLoading();
    }

    public boolean validate(){

        if(email.getText().toString().equals("") ||
                password.getText().toString().equals("")){
            Toast.makeText(this, "Por favor, informe o e-mail e senha para continuar!", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public void onSuccess(ForgotResponse forgotResponse) {
        hideLoading();
        SharedHelper.putKey(this, Constants.SharedPref.TXT_EMAIL, email.getText().toString());
        SharedHelper.putKey(this, Constants.SharedPref.OTP, String.valueOf(forgotResponse.getProvider().getOtp()));
        SharedHelper.putKey(this, Constants.SharedPref.ID, String.valueOf(forgotResponse.getProvider().getId()));
        Toasty.success(this, forgotResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        startActivity(new Intent(this, ResetActivity.class));
    }

    @Override
    public void onSuccess(User user) {
        hideLoading();
        SharedHelper.putKey(this, Constants.SharedPref.ACCESS_TOKEN, user.getAccessToken());
        SharedHelper.putKey(this, Constants.SharedPref.USER_ID,
                String.valueOf(user.getId()));
        SharedHelper.putKey(this, Constants.SharedPref.LOGGGED_IN, "true");
        Toasty.success(activity(), getString(R.string.login_out_success), Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();


    }

    @Override
    public void onError(Throwable e) {
        hideLoading();
        if (e != null)
            TAG = "PasswordActivity";
        onErrorBase(e);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }
}